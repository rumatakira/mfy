<?php

/** @author Kirill Lapchinsky rumatakira74@gmail.com */

namespace backend\controllers;

use Yii;
use Faker;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
use common\models\User;
use common\traits\CommonTrait;
use yii\imagine\Image;

define('__VENDOR__', dirname(dirname(__DIR__)));
require_once(__VENDOR__ . "/vendor/fzaninotto/faker/src/autoload.php");

/**
 * Truncate tables {{user}}, {{participants}}, {{userauthlog}} and populates
 * {{user} with 33 fakes users.
 */
class FakerController extends Controller {

    /**
     * Only root access. Admins - denied!
     * 
    
    public function behaviors () {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                        [
                        'allow' => true,
                        'roles' => ['root'],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    throw new \Exception('You have no permission to perfom this action!');
                },
            ],
        ];
    }
*/
    /**
     * Populates database with faker users. Part 1, because shared hosting
     * server php timeout.
     * 
     * @return goHome()
     */
    public function actionFaker1() {
        $faker = Faker\Factory::create();
        $faker->seed(1234); // ucomment for the same fake data
        //Yii::$app->db->createCommand()->truncateTable('user')->execute();
        //Yii::$app->db->createCommand()->truncateTable('participants')->execute();
        //Yii::$app->db->createCommand()->truncateTable('userauthlog')->execute();
        FileHelper::copyDirectory(dirname(__DIR__) . '/data/ForFaker', dirname(__DIR__) . '/data/FakerPicsTmp');
        for ($i = 0; $i < 10; $i++) {
            $user = new User();
            $user->username = $faker->userName;
            $user->email = $faker->email;
            $user->first_name = $faker->firstName;
            $user->last_name = $faker->lastName;
            $user->sex = $faker->randomElement($array = array('Female', 'Man', 'Other gender'));
            $user->birth_date = $faker->date($format = 'Y-m-d', $max = '-12 years');
            $user->country = $faker->country;
            $user->area = $faker->state;
            $user->city = $faker->city;
            $user->password_hash = Yii::$app->getSecurity()->generatePasswordHash('Demo2016');
            $user->secret_question = 'Your mother maiden name?';
            $user->secret_question_answer = Yii::$app->getSecurity()->generatePasswordHash('Blank');
            $user->auth_key = Yii::$app->getSecurity()->generateRandomString();
            $user->status = 10;
            $user->photo_extension = 'jpg';
            if ($i%2 != 0) {
                $user->show_email = 1;
                $user->photo_verified = 1;
            } else {
                $user->photo_verified = 0;
            }

            $filename = dirname(__DIR__) . '/data/FakerPicsTmp/' . "$i" . '.' . 'jpg';
            Image::thumbnail($filename, $width = 550, $height = null)->save($filename);
            CommonTrait::setPhotoBorder($user, $filename);
            Image::watermark($filename, $_SERVER['DOCUMENT_ROOT'] . '/img/watermark.png', [20, 20])->save($filename);
            CommonTrait::prepareImageForDatabase($user, $filename);
            $user->save();
        }
        Yii::$app->session->setFlash('success', 'Database populated! First part.');

        return $this->goHome();
    }
    
    /**
     * Populates database with faker users. Part 2, because shared hosting
     * server php timeout.
     * 
     * @return goHome()
     */    
    public function actionFaker2() {
        $faker = Faker\Factory::create();
        $faker->seed(6789); // ucomment for the same fake data
        for ($i = 10; $i < 20; ++$i) {
            $user = new User();
            $user->username = $faker->userName;
            $user->email = $faker->email;
            $user->first_name = $faker->firstName;
            $user->last_name = $faker->lastName;
            $user->sex = $faker->randomElement($array = array('Female', 'Man', 'Other gender'));
            $user->birth_date = $faker->date($format = 'Y-m-d', $max = '-12 years');
            $user->country = $faker->country;
            $user->area = $faker->state;
            $user->city = $faker->city;
            $user->password_hash = Yii::$app->getSecurity()->generatePasswordHash('Demo2016');
            $user->secret_question = 'Your mother maiden name?';
            $user->secret_question_answer = Yii::$app->getSecurity()->generatePasswordHash('Blank');
            $user->auth_key = Yii::$app->getSecurity()->generateRandomString();
            $user->status = 10;
            $user->photo_extension = 'jpg';
            if ($i%2 != 0) {
                $user->show_email = 1;
                $user->photo_verified = 1;
            } else {
                $user->photo_verified = 0;
            }

            $filename = dirname(__DIR__) . '/data/FakerPicsTmp/' . "$i" . '.' . 'jpg';
            Image::thumbnail($filename, $width = 550, $height = null)->save($filename);
            CommonTrait::setPhotoBorder($user, $filename);
            Image::watermark($filename, $_SERVER['DOCUMENT_ROOT'] . '/img/watermark.png', [20, 20])->save($filename);
            CommonTrait::prepareImageForDatabase($user, $filename);
            $user->save();
        }

        Yii::$app->session->setFlash('success', 'Database populated! Second part.');

        return $this->goHome();
    }
    
        /**
     * Populates database with faker users. Part 3, because shared hosting
     * server php timeout.
     * 
     * @return goHome()
     */
    public function actionFaker3() {
        $faker = Faker\Factory::create();
        $faker->seed(5678); // ucomment for the same fake data
        for ($i = 20; $i < 32; ++$i) {
            $user = new User();
            $user->username = $faker->userName;
            $user->email = $faker->email;
            $user->first_name = $faker->firstName;
            $user->last_name = $faker->lastName;
            $user->sex = $faker->randomElement($array = array('Female', 'Man', 'Other gender'));
            $user->birth_date = $faker->date($format = 'Y-m-d', $max = '-12 years');
            $user->country = $faker->country;
            $user->area = $faker->state;
            $user->city = $faker->city;
            $user->password_hash = Yii::$app->getSecurity()->generatePasswordHash('Demo2016');
            $user->secret_question = 'Your mother maiden name?';
            $user->secret_question_answer = Yii::$app->getSecurity()->generatePasswordHash('Blank');
            $user->auth_key = Yii::$app->getSecurity()->generateRandomString();
            $user->status = 33;
            $user->photo_extension = 'jpg';
            if ($i%2 != 0) {
                $user->show_email = 1;
            }

            $filename = dirname(__DIR__) . '/data/FakerPicsTmp/' . "$i" . '.' . 'jpg';
            Image::thumbnail($filename, $width = 550, $height = null)->save($filename);
            CommonTrait::setPhotoBorder($user, $filename);
            Image::watermark($filename, $_SERVER['DOCUMENT_ROOT'] . '/img/watermark.png', [20, 20])->save($filename);
            CommonTrait::prepareImageForDatabase($user, $filename);
            $user->save();
        }

        FileHelper::removeDirectory(dirname(__DIR__) . '/data/FakerPicsTmp');
        Yii::$app->session->setFlash('success', 'Database populated! Third part.');

        return $this->goHome();
    }
    
    /** 
     * Populates users participants
     * 
     * @return goHome()
     */
    public function actionFakerParticipants() {
        $faker = Faker\Factory::create();
        $users = User::find()->all();
        for ($i = 0; $i < 32; $i++) {
            for ($j = 0; $j < 32; $j++) {
                if ($j != $i) {
                    $users[$i]['participants'] = $users[$i]['participants'] . $users[$j]['username'] . ':' . $faker->randomElement($array = array('Family', 'Friends', 'Сolleague', 'Team1', 'Team2', 'Blocked')) . ':';
                }
            }
            $users[$i]['participants'] = substr($users[$i]['participants'], 0, -1);
            $users[$i]->save();
        }
        Yii::$app->session->setFlash('success', 'Database populated! Participants part.');

        return $this->goHome();
    }

    /**
     * Create rumatakira admin user
     * 
     * @return goHome()
     */
    public function actionFakerRumatakira() {
        $user = new User();
        $user->username = 'rumatakira';
        $user->email = 'support@mafiaonlinegame.com';
        $user->first_name = 'Kira';
        $user->last_name = 'Rumata';
        $user->sex = 'Female';
        $user->birth_date = '1973-11-20';
        $user->country = 'Montenegro';
        $user->area = 'Budva';
        $user->city = 'Budva';
        $user->password_hash = Yii::$app->getSecurity()->generatePasswordHash('Medvezhonka08'); // set the password !!!
        $user->secret_question = 'Your favorite meal?';
        $user->secret_question_answer = Yii::$app->getSecurity()->generatePasswordHash(''); // set the answer !!!
        $user->auth_key = Yii::$app->getSecurity()->generateRandomString();
        $user->status = 33;
        $user->photo_extension = 'jpg';
        $filename = $_SERVER['DOCUMENT_ROOT'] . '/img/avatar_woman.jpg';
        CommonTrait::prepareImageForDatabase($user, $filename);
        $user->save();
        Yii::$app->session->setFlash('success', 'Rumatakira admin user created.');

        return $this->goHome();
    }
    
}
