<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use samdark\sitemap\Sitemap;
use samdark\sitemap\Index;

/**
 * Creates sitemap.xml
 */
class SitemapController extends Controller {

    public function actionSitemap() {

        // create sitemap
        $sitemap = new Sitemap(Yii::getAlias('@webroot/../sitemap.xml'));

        // add some URLs
        $sitemap->addItem('http://http://mafiaonlinegame.com/index.php?r=site%2Findex', time(), Sitemap::DAILY);
        $sitemap->addItem('http://http://mafiaonlinegame.com/index.php?r=site%2Flogin', time(), Sitemap::DAILY);
        $sitemap->addItem('http://http://mafiaonlinegame.com/index.php?r=site%2Fsignup', time(), Sitemap::DAILY);
        $sitemap->addItem('http://mafiaonlinegame.com/index.php?r=site%2Fcontact', time(), Sitemap::DAILY);
        $sitemap->addItem('http://mafiaonlinegame.com/index.php?r=site%2Fsite-map', time(), Sitemap::DAILY);

        // write it
        $sitemap->write();

        Yii::$app->session->setFlash('success', 'Sitemap file created!');

        return $this->goHome();

        /*
          // add some URLs
          $sitemap->addItem('http://example.com/mylink1');
          $sitemap->addItem('http://example.com/mylink2', time());
          $sitemap->addItem('http://example.com/mylink3', time(), Sitemap::HOURLY);
          $sitemap->addItem('http://example.com/mylink4', time(), Sitemap::DAILY, 0.3);

          // get URLs of sitemaps written
          $sitemapFileUrls = $sitemap->getSitemapUrls('http://example.com/');

          // create sitemap for static files
          $staticSitemap = new Sitemap(__DIR__ . '/sitemap_static.xml');

          // add some URLs
          $staticSitemap->addItem('http://example.com/about');
          $staticSitemap->addItem('http://example.com/tos');
          $staticSitemap->addItem('http://example.com/jobs');

          // write it
          $staticSitemap->write();

          // get URLs of sitemaps written
          $staticSitemapUrls = $staticSitemap->getSitemapUrls('http://example.com/');

          // create sitemap index file
          $index = new Index(__DIR__ . '/sitemap_index.xml');

          // add URLs
          foreach ($sitemapFileUrls as $sitemapUrl) {
          $index->addSitemap($sitemapUrl);
          }

          // add more URLs
          foreach ($staticSitemapUrls as $sitemapUrl) {
          $index->addSitemap($sitemapUrl);
          }

          // write it
          $index->write();
         */
    }

}
