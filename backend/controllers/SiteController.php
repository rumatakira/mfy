<?php

/** @author Kirill Lapchinsky rumatakira74@gmail.com */

namespace backend\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Backend site controller.
 */
class SiteController extends Controller {
/*
    public function behaviors () {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                        [
                        'allow' => true,
                        'roles' => ['root', 'admin'],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    throw new \Exception('You have no permission to access this area!');
                },
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
 */

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex() {
        return $this->render('index');
    }

    /**
     * ! This is not mistake !
     * Set border to template user photos according user sex.
     */
    public
    function actionSetPhotoBorders() {
        //copy files from main avatar
        copy($_SERVER['DOCUMENT_ROOT'] . '/img/avatar.jpg', $_SERVER['DOCUMENT_ROOT'] . '/img/avatar_woman.jpg');
        copy($_SERVER['DOCUMENT_ROOT'] . '/img/avatar.jpg', $_SERVER['DOCUMENT_ROOT'] . '/img/avatar_man.jpg');
        copy($_SERVER['DOCUMENT_ROOT'] . '/img/avatar.jpg', $_SERVER['DOCUMENT_ROOT'] . '/img/avatar_other_gender.jpg');
        // set borders and save files
        \yii\imagine\Image::frame($_SERVER['DOCUMENT_ROOT'] . '/img/avatar_woman.jpg', 16, '#FF99CC')->save($_SERVER['DOCUMENT_ROOT'] . '/img/avatar_woman.jpg');
        \yii\imagine\Image::frame($_SERVER['DOCUMENT_ROOT'] . '/img/avatar_man.jpg', 16, '#0066FF')->save($_SERVER['DOCUMENT_ROOT'] . '/img/avatar_man.jpg');
        \yii\imagine\Image::frame($_SERVER['DOCUMENT_ROOT'] . '/img/avatar_other_gender.jpg', 16, '#CC00FF')->save($_SERVER['DOCUMENT_ROOT'] . '/img/avatar_other_gender.jpg');

        Yii::$app->session->setFlash('success', 'Done!');
        return $this->render('index');
    }

}
