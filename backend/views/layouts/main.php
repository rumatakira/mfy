<?php
/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="wrap">
            <?php
            NavBar::begin([
                'brandLabel' => 'Mafia online role game',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);

            $menuItems[] = ['label' => 'Backend', 'url' => ['/site/index']];
            $menuItems[] = ['label' => 'Frontend', 'url' => '@web/../'];
            $menuItems[] = [
                'label' => 'Tests',
                'items' => [
                    [
                        'label' => 'Faker database population1',
                        'url' => ['/faker/faker1'],
                    ],
                    [
                        'label' => 'Faker database population2',
                        'url' => ['/faker/faker2'],
                    ],
                    [
                        'label' => 'Faker database population3',
                        'url' => ['/faker/faker3'],
                    ],
                    [
                        'label' => 'Faker participants population',
                        'url' => ['/faker/faker-participants'],
                    ],
                    [
                        'label' => 'Create rumatakira admin user',
                        'url' => ['/faker/faker-rumatakira'],
                    ],
                    [
                        'label' => 'Set borders to template user photos',
                        'url' => ['/site/set-photo-borders'],
                    ],
                ],
            ];
            $menuItems[] = [
                'label' => 'Site',
                'items' => [
                    [
                        'label' => 'Create Sitemap',
                        'url' => ['/sitemap/sitemap'],
                    ],
                ],
            ];
            $menuItems[] = [
                'label' => 'Database',
                'items' => [
                    [
                        'label' => 'Clean Database',
                        'url' => ['/sitemap/sitemap'],
                    ],
                ],
            ];

            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
            ]);
            NavBar::end();
            ?>

            <div class="container">
                <?=
                Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ])
                ?>
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

                <p class="pull-right"><?= Yii::powered() ?></p>
            </div>
        </footer>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
