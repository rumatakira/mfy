<?php

use yii\db\Migration;

/**
 * Migration add profile fields to {{user}} database.
 */
class m151207_221101_add_profile_fields_to_user_table extends Migration {

    public function safeUp () {
        $arr = [
            'first_name' => $this->string(32)->notNull(),
            'last_name' => $this->string(32)->notNull(),
            'sex' => $this->string(32)->notNull(),
            'birth_date' => $this->date()->notNull(),
            'country' => $this->string(128)->notNull(),
            'area' => $this->string(32)->notNull(),
            'city' => $this->string(32)->notNull(),
            'secret_question' => $this->string()->notNull(),
            'secret_question_answer' => $this->string()->notNull(),
            'participants' => $this->text(),
        ];

        foreach ($arr as $key => $value) {
            $this->addColumn('{{%user}}', $key, $value);
        }
        //$this->addColumn('{{%user}}', 'participants', 'TEXT');
        $this->createIndex('country', '{{%user}}', 'country');
        $this->createIndex('city', '{{%user}}', 'city');
    }

    public function safeDown () {
        $arr = [
            'first_name',
            'last_name',
            'sex',
            'birth_date',
            'country',
            'area',
            'city',
            'secret_question',
            'secret_question_answer',
            'participants',
        ];

        foreach ($arr as $value) {
            $this->dropColumn('{{%user}}', $value);
        }
    }

}
