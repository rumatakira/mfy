<?php

use yii\db\Migration;

/**
 * Initial migration
 */
class m130524_201442_init extends Migration {

    public function safeUp () {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string(32)->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string(128)->notNull()->unique(),
            'new_email' => $this->string(128),
            'status' => $this->smallInteger(1)->notNull()->defaultValue(5),
            'suspended_till' => $this->dateTime(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            ], $tableOptions);
    }

    public function safeDown () {
        $this->dropTable('{{%user}}');
    }

}
