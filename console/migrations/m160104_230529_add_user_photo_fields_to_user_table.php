<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Migration adds user photo fields to database
 */
class m160104_230529_add_user_photo_fields_to_user_table extends Migration {

    public function safeUp () {
        $arr = [
            'photo' => $this->text(),
            'photo_extension' => $this->string(128),
            'photo_verified' => $this->smallInteger(1)->notNull()->defaultValue(-1),
            'identity_verified' => $this->smallInteger(1),
            'notifications' => $this->string(6)->notNull()->defaultValue('Always'),
            'notifications_from' => $this->string(9)->notNull()->defaultValue('All users'),
            'show_email' => $this->smallInteger(1)->notNull()->defaultValue(0),
        ];

        foreach ($arr as $key => $value) {
            $this->addColumn('{{%user}}', $key, $value);
        }
    }

    public function safeDown () {
        $arr = [
            'photo',
            'photo_extension',
            'photo_verified',
            'identity_verified',
            'notifications',
            'notificationsFrom',
            'show_email',
        ];

        foreach ($arr as $value) {
            $this->dropColumn('{{%user}}', $value);
        }
    }

}
