<?php

use yii\db\Migration;

/**
 * Migration creates {{participants}} table.
 */
class m160224_025438_create_participants_table extends Migration {

    public function safeUp () {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%participants}}', [
            'id' => $this->primaryKey(),
            'userId' => $this->integer()->notNull(),
            'participant' => $this->string(32)->notNull(),
            'category' => $this->string()->notNull()->defaultValue('Uncategorized'),
            ], $tableOptions);

        $this->createIndex('userId', '{{%participants}}', 'userId');
    }

    public function safeDown () {
        $this->dropTable('participants');
    }

}
