<?php

use yii\db\Migration;

/**
 * Migration for yii2tech_authlog.
 */
class m160304_234748_add_yii2tech_authlog extends Migration {

    public function safeUp () {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%userauthlog}}', [
            'id' => $this->primaryKey(),
            'userId' => $this->integer(),
            'date' => $this->integer(),
            'cookieBased' => $this->boolean(),
            'duration' => $this->integer(),
            'error' => $this->string(),
            'ip' => $this->string(),
            'host' => $this->string(),
            'url' => $this->string(),
            'userAgent' => $this->string(),
            ], $tableOptions);
    }

    public function safeDown () {
        $this->dropTable('UserAuthLog');
    }

}
