<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * User Auth Log
 */
class UserAuthLog extends ActiveRecord {

    /**
     * @return string название таблицы, сопоставленной с этим ActiveRecord-классом.
     */
    public static function tableName() {
        return 'userauthlog';
    }

    public function getUserViaId() {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }

}
