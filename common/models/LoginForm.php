<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii2tech\authlog\AuthLogLoginFormBehavior;

/**
 * Login form.
 */
class LoginForm extends Model {

    public $username;
    public $password;
    public $rememberMe = true;
    public $verificationCodeCaptcha;
    public $user;
    public $userTmp;

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'user';
    }

    public function behaviors() {
        return [
            'authLog' => [
                'class' => AuthLogLoginFormBehavior::className(),
                'findIdentity' => 'getUser',
                'verifyRobotAttribute' => 'verificationCodeCaptcha',
                'deactivateIdentity' => function ($identity) {
                    return \yii\web\Controller::goHome();
                },
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            ['username', 'validateUser'],
            // password is validated by validatePassword()
            ['password', 'validateUserPassword'],
            ['verificationCodeCaptcha', 'safe'],
        ];
    }

    /**
     * Validates the user.
     *
     * @param string $attribute the attribute currently being validated
     * @param array  $params    the additional name-value pairs given in the rule
     */
    public function validateUser($attribute, $params) {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user) {
                $this->addError($attribute, 'There is no such user.');
            } elseif ($user->status == 5) {
                $this->addError($attribute, 'Please confirm your email first.');
            } elseif ($user->status == 1) {
                $this->addError($attribute, 'This user was suspended till: ' . date('d F Y H:i:s', $user->suspended_till));
            } elseif ($user->status == -1) {
                $this->addError($attribute, 'This user was blocked!');
                \Yii::$app->session->destroy();
                \Yii::$app->response->cookies->removeAll();
//                \Yii::$app->response->statusCode = 403;
//                \Yii::$app->response->send();
            }
        }
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array  $params    the additional name-value pairs given in the rule
     */
    public function validateUserPassword($attribute, $params) {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login() {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]].
     *
     * @return User|null
     */
    public function getUser() {
        $userTmp = User::findByUsername($this->username);
        if (!$userTmp) {
            $this->user = User::findByEmail($this->username);
        } else {
            $this->user = $userTmp;
        }

        return $this->user;
    }

}
