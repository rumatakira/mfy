<?php
/** @author Kirill Lapchinsky rumatakira74@gmail.com */

namespace common\traits;

use Yii;

/**
* Implements common trait used by application.
*/
trait CommonTrait {

    /**
     * Generates unique user-friendly random password containing at least
     * two lower case letters, two upper case letters, two digits, two special
     * characters. So minimal length is 8. Default 15 characters. The remaining
     * characters in the password are chosen at random from the sets. Verify does
     * password unique in {{user}} table. Return or regenerate the password.
     *
     * @param $length
     *
     * @return string
     */
    public static
        function generatePassword ($length = 15) {
        do {
            $sets = [
                'abcdefghjkmnpqrstuvwxyz',
                'abcdefghjkmnpqrstuvwxyz',
                'ABCDEFGHJKMNPQRSTUVWXYZ',
                'ABCDEFGHJKMNPQRSTUVWXYZ',
                '0123456789',
                '0123456789',
                '!@#$%^&*-',
                '!@#$%^&*-',
            ];
            $all = '';
            $password = '';
            foreach ($sets as $set) {
                $password .= $set[array_rand(str_split($set))];
                $all .= $set;
            }

            $all = str_split($all);
            for ($i = 0; $i < $length - count($sets); ++$i) {
                $password .= $all[array_rand($all)];
            }

            $password = str_shuffle($password);
        }
        while (\common\models\User::findByPassword($password));

        return $password;
    }

    /**
     * Creates and set user roles.
     *
     * @param reference $user - ActiveRecord object
     */
    public static
        function setUserRoles (&$user) {
        $auth = Yii::$app->authManager;
        $roles = $auth->getAssignments($user->id);
        if (empty($roles)) {
            if ($user->username == 'rumatakira') {
                $root = $auth->createRole('root');
                $auth->add($root);
                $auth->assign($root, $user->id);
            }
            elseif ($user->username == 'rozuman') {
                $admin = $auth->createRole('admin');
                $auth->add($admin);
                $auth->assign($admin, $user->id);
            }
            elseif ($user->username == 'nikitina') {
                $admin = $auth->createRole('admin');
                $auth->add($admin);
                $auth->assign($admin, $user->id);
            }
        }
    }

    /**
     * Rename user status from int to understandable string.
     *
     * @param reference $user - ActiveRecord object
     */
    public static
        function setGlobalsStatus ($status) {
        switch ($status) {
            case -1:
                $GLOBALS['userStatus'] = 'THIS USER WAS BLOCKED BY SITE ADMINISTRATION.';
                $GLOBALS['statusColor'] = '#993300';
                break;
            case 0:
                $GLOBALS['userStatus'] = 'This account was deleted by user and soon will be removed from the base.';
                $GLOBALS['statusColor'] = '#000000';
                break;
            case 10:
                $GLOBALS['userStatus'] = 'Email verified, personal photo not approved & identity not verified.';
                $GLOBALS['statusColor'] = '#FF0000';
                break;
            case 23:
                $GLOBALS['userStatus'] = 'Email verified & personal photo approved, identity not verified.';
                $GLOBALS['statusColor'] = '#FF8000';
                break;
            case 33:
                $GLOBALS['userStatus'] = 'Email, personal photo, identity verified & approved.';
                $GLOBALS['statusColor'] = '#00CC00';
                break;
        }
    }

    /**
     * Set user status according database record.
     * Needed if user return after deleted his/her acount.
     *
     * @param reference $user - ActiveRecord object
     */
    public static
        function statusRestore (&$user) {
        if ($user->identity_verified == 1) {
            $user->status = 33;
        }
        elseif ($user->photo_verified == 1) {
            $user->status = 23;
        }
        else {
            $user->status = 10;
        }
    }

    /**
     * Choosing standart user photo for use according user sex.
     *
     * @param reference $user - ActiveRecord object
     */
public static
        function setBasePhoto (&$user) {
        switch ($user->sex) {
            case 'Female':
                $sourceImage = $_SERVER['DOCUMENT_ROOT'] . '/img/avatar_woman.jpg';
                break;
            case 'Man':
                $sourceImage = $_SERVER['DOCUMENT_ROOT'] . '/img/avatar_man.jpg';
                break;
            case 'Other gender':
                $sourceImage = $_SERVER['DOCUMENT_ROOT'] . '/img/avatar_other_gender.jpg';
                break;
        }
        $user->photo_extension = 'jpg';
        $user->photo_verified = -1; //default photo
        self::prepareImageForDatabase ($user, $sourceImage);
    }

    /**
     * Prepare user photo file for saving to database.
     *
     * @param reference $user - ActiveRecord object
     * @param null $filename - full path to photo file 
     */
    public static
        function prepareImageForDatabase (&$user, $filename = null) {
        if (!$filename) {
            $handle = fopen($_SERVER['DOCUMENT_ROOT'] . '/tmp/' . "$user->id" . '.' . $user->photo_extension, 'r+');
            $contents = fread($handle, filesize($_SERVER['DOCUMENT_ROOT'] . '/tmp/' . "$user->id" . '.' . $user->photo_extension));
            fclose($handle);
        } else {
            $handle = fopen($filename, 'r+');
            $contents = fread($handle, filesize($filename));
            $encoded = base64_encode($contents);
            fclose($handle);
        }
            $user->photo = base64_encode($contents);
    }

    /**
     * Set border to user photo according user sex.
     *
     * @param reference $user - ActiveRecord object
     * @param null $filename - full path to photo file
     */
    public static
        function setPhotoBorder (&$user, $filename = null) {
        switch ($user->sex) {
            case 'Female':
                $borderColor = '#FF99CC';
                break;
            case 'Man':
                $borderColor = '#0066FF';
                break;
            case 'Other gender':
                $borderColor = '#CC00FF';
                break;
        }
        if (!$filename) {
            \yii\imagine\Image::frame($_SERVER['DOCUMENT_ROOT'] . '/tmp/' . "$user->id" . '.' . $user->photo_extension, 16, $borderColor)->save($_SERVER['DOCUMENT_ROOT'] . '/tmp/' . "$user->id" . '.' . $user->photo_extension);
        } else {
            \yii\imagine\Image::frame($filename, 16, $borderColor)->save($filename);
        }
    }

    /**
     * Read file @common/data/blockedIPs.txt to array.
     *
     * @return array $IPs
     */
    public static
        function getBlockedIPs () {
        $file = Yii::getAlias('@common/data/') . 'blockedIPs.txt';
        $IPs = file($file, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

        return array_map('trim', $IPs);
    }

    /**
     * Explode user participants and categories from the table {{%user}},
     * were they are permanently stored in TEXT field [[participants]]
     * in format (participant:category), delete all rows in {{%participants}} by id
     * and add new filled rows to the table {{%participants}}. Note! Table
     * {{%participants}} is used only for the logged in users. During logout all data
     * by current id must be deleted in {{%participants}}.
     *
     * @param reference $user - ActiveRecord object
     */
    public static
        function participantsExplode (&$user) {
        if (!$user->participants) {
            return;
        }
        else {
            Yii::$app->db->createCommand()->delete('{{participants}}', "[[userId]] = :id ")
                ->bindValue(':id', $user->id)
                ->execute();
            $explodedparticipants = \yii\helpers\StringHelper::explode($user->participants, ':', true, true);
            $i = 0;
            $count = count($explodedparticipants);
            while ($i < $count) {
                Yii::$app->db->createCommand()->insert('{{participants}}', [
                    '[[userId]]' => $user->id,
                    '[[participant]]' => $explodedparticipants[$i],
                    '[[category]]' => $explodedparticipants[++$i],
                ])->execute();
                ++$i;
            }
        }
    }

    /**
     * Implode user participants and categories from the table {{%participants}} to string fin format (participant:category:participant:category) and store it in TEXT field [[participants]] table {{user}}. Delete all rows in {{%participants}} by $user->id.
     * Note! Table {{participants}} used only for the logged in users.
     *
     * @param reference $user - ActiveRecord object
     *
     * @return true/false
     */
    public static
        function participantsImplode (&$user) {
        $implodedparticipants = Yii::$app->db->createCommand("SELECT CONCAT_WS(':', participant, category) FROM {{participants}} WHERE [[userId]] = :id ORDER BY [[participant]]")
            ->bindValue(':id', $user->id)
            ->queryALL();
        if (!empty($implodedparticipants)) {
            $i = 0;
            $count = count($implodedparticipants);
            $str = '';
            while ($i < $count) {
                // MySQL version
                if (Yii::$app->db->driverName === 'mysql') {
                $str = $str . $implodedparticipants[$i]['CONCAT_WS(\':\', participant, category)'] . ':';
                // PostgreSQL version
                } elseif (Yii::$app->db->driverName === 'pgsql') {
                $str = $str . $implodedparticipants[$i]['concat_ws'] . ':';
                }
                ++$i;
            }
            $str = substr($str, 0, -1);
            $user->participants = $str;
            Yii::$app->db->createCommand()->delete('{{participants}}', "[[userId]] = :id ")
                ->bindValue(':id', $user->id)
                ->execute();

            return true;
        }
        else {
            return;
        }
    }

}
