<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$validateLink = Yii::$app->urlManager->createAbsoluteUrl(['site/email-confirmation', 'id' => $user->id, 'token' => $user->auth_key]);
//активировать после настройки openSSL
//$validateLink = Yii::$app->urlManager->createAbsoluteUrl(['site/index', 'token' => $user->auth_key], 'https');
?>
<div class="password-reset">
    <p>Hello <?= Html::encode($user->username) ?>,</p>
    <p>
        Email address verification was requested on the <?= Html::a(Yii::$app->name, Yii::getAlias('@mafiaSite')); ?>. If you did not performed this request, please ignore this email.
    </p>
    <p>To verify this email address, open the following link:</p>
    <p><?= Html::a(Html::encode($validateLink), $validateLink) ?></p>
    <p>
        If the link does not open correctly, copy-paste it to the browser's address bar.
    </p>
</div>
