<?php

Yii::setAlias('common', dirname(__DIR__));
Yii::setAlias('frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@mafiaSite', 'http://mafiaonlinegame.com');
Yii::setAlias('@mafiaSiteForum', 'http://forum.mafiaonlinegame.com');
Yii::setAlias('@mafiaSiteMission', 'https://www.indiegogo.com/projects/mafia-online-role-game#/');
Yii::setAlias('@mafiaSiteAbout', 'https://www.indiegogo.com/projects/mafia-online-role-game#/');
Yii::setAlias('@mafiaSiteBugs', 'http://forum.mafiaonlinegame.com');
Yii::setAlias('@mafiaSiteRules', 'http://forum.mafiaonlinegame.com');
Yii::setAlias('@mafiaGameRules', 'http://en.wikipedia.org/wiki/Mafia_(party_game)');
Yii::setAlias('@mafiaGamePlay', 'http://en.wikipedia.org/wiki/Mafia_(party_game)');
