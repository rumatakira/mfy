<?php

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
//    'controllerMap' => [
//        'fixture' => [
//            'class' => 'yii\faker\FixtureController',
//        ],
//    ],
    'bootstrap' => ['log', 'forum_eng'],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
            'assignmentFile' => '@common/rbac/assignments.php',
            'itemFile' => '@common/rbac/items.php',
        ],
    ],
    'modules' => [
        'forum_eng' => [
            'class' => 'bizley\podium\Podium',
            'userComponent' => 'user',
            'adminId' => 99,
        ],
    ],
];
