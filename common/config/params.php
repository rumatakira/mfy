<?php

return [
    'adminEmail' => 'admin@mafiaonlinegame.com',
    'supportEmail' => 'support@mafiaonlinegame.com',
    'user.passwordResetTokenExpire' => 3600,
];
