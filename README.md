# Online role game, known as "Mafia", based on video chart (WebRTC) integrated to the browsers like Chrome, Firefox and Opera.#
## **Description:**
Working example can be viewed [here](http://mafiaonlinegame.com//).

Dockerized version.

Project implementation:

[![Yii2](https://img.shields.io/badge/Powered_by-Yii_Framework-green.svg?style=flat)](http://www.yiiframework.com/) + Redis + HHVM (PHP-HACKlang) + MySQL + Bootstrap + Javascript + Angular + WebRTC + RUSTlang.

For that moment all documentation are in the source  files and can be build by phpDocumentor tool.


