<?php

/** @author Kirill Lapchinsky rumatakira74@gmail.com */

namespace frontend\controllers;

use Yii;
use common\models\User;
use common\traits\CommonTrait;
use frontend\models\SearchUsersForm;
use frontend\models\ParticipantsForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * SearchController implements the CRUD actions for UserSearch model.
 */
class SearchUsersController extends Controller {

    /**
     * Lists all UserSearch models.
     *
     * @return mixed
     */
    public function actionIndex($viewOnlyPremium = null, $viewOnlyApprovedPhoto = null, $viewOnlyWithPhoto = null) {
        $additionalParams = ['viewOnlyPremium' => $viewOnlyPremium, 'viewOnlyApprovedPhoto' => $viewOnlyApprovedPhoto, 'viewOnlyWithPhoto' => $viewOnlyWithPhoto];
        $searchModel = new SearchUsersForm();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $additionalParams);

        return $this->render(
        'index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]
        );
    }

//end actionIndex()

    /**
     * Displays a single UserSearch model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id) {
        $user = $this->findModel($id);
        CommonTrait::setGlobalsStatus($user->status);

        return $this->render(
        'view', ['model' => $user]
        );
    }

//end actionView()

    /**
     * Add a single UserSearch model to the table participant.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionAdd($id) {
        return $this->redirect(['/participants/create', 'id' => $id, 'redirectFromUsersSearch' => true]);
    }

//end actionAdd()

    /**
     * Block a single UserSearch model by adding to the table participant with category 'Blocked'.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionBlock($id) {
        $userToBlock = $this->findModel($id);
        $model = new ParticipantsForm();
        $model->scenario = ParticipantsForm::SCENARIO_CREATE;
        $model->participant = $userToBlock->username;
        $model->userId = Yii::$app->user->getId();
        $model->category = 'Blocked';
        try {
            $model->save();
            Yii::$app->session->setFlash('success', "User $userToBlock->username was blocked and can't be able to play or communicate with you.");
        } catch (ErrorException $e) {
            Yii::$app->session->setFlash('error', 'Oops... Something went wrong. There was server error during blocking user. Try again later. If the mistake will remain - please contact us.');

            return false;
        }

        return $this->redirect(['index']);
    }

//end actionBlock()

    /**
     * Finds the UserSearch model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return UserSearch the loaded model
     *
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

//end findModel()
}

//end class
