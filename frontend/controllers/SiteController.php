<?php

/** @author Kirill Lapchinsky rumatakira74@gmail.com */

namespace frontend\controllers;

use Yii;
use common\models\User;
use common\models\LoginForm;
use common\traits\CommonTrait;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\ChangePasswordForm;
use frontend\models\SignupForm;
use frontend\models\EditProfileForm;
use frontend\models\UpdatePhotoForm;
use frontend\models\DeleteAccountForm;
use frontend\models\ContactForm;
use frontend\models\SettingsForm;
use yii\base\InvalidParamException;
use yii\base\ErrorException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * Site controller.
 */
class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [
                    'login',
                    'logout',
                    'signup',
                ],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => [
                            'login',
                            'signup',
                        ],
                        'allow' => false,
                        'ips' => CommonTrait::getBlockedIPs(),
                    // NOTE! With Yii 2.0.7 working only IPv6 addresses
                    ],
                    [
                        'actions' => [
                            'login',
                            'signup',
                        ],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

//end behaviors()

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => ['class' => 'yii\web\ErrorAction'],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

//end actions()

    /**
     * Redirecting user to the Signup if guest, or to the Gameplay page.
     */
    public function actionPlay() {
        if (\Yii::$app->user->isGuest) {
            return $this->redirect(['signup']);
        } else {
            return $this->redirect(['profile']);
        }
    }

//end actionPlay()

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex() {
        return $this->render('index');
    }

//end actionIndex()

    /**
     * Displays different site map pages for guests and users.
     *
     * @return mixed
     */
    public function actionSiteMap() {
        if (\Yii::$app->user->isGuest) {
            return $this->render('siteMapGuest');
        } else {
            return $this->render('siteMapUser');
        }
    }

//end actionSiteMap()

    /**
     * Displays contact page for guests.
     *
     * @return mixed
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(\Yii::$app->params['supportEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render(
            'contact', ['model' => $model]
            );
        }
    }

//end actionContact()

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin() {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) and $model->login()) {
            $user = Yii::$app->user->getIdentity();
            CommonTrait::setUserRoles($user);
            try {
                CommonTrait::participantsExplode($user);
                if ($user->status == 0) {
                    CommonTrait::statusRestore($user);
                    $user->save();
                    Yii::$app->session->setFlash('success', 'Welcome back ' . $user->username . '! Your account was restored!');
                }
            } catch (ErrorException $e) {
                $flash = array('error' => 'Oops... Something went wrong. There was server error during preparing operations. Try to reload the main page and login again. If the mistake will remain - please contact us.');

                self::actionLogout($flash);
            }

            return $this->goBack();
        } else {
            return $this->render(
            'login', ['model' => $model]
            );
        }//end if
    }

//end actionLogin()

    /**
     * Logs out the current user. Delete all rows with
     * current user->id from {{participants}}, i
     * mplode them and save to {{user}} [[participants]].
     *
     * @return mixed
     */
    public function actionLogout($flash = null) {
        $user = Yii::$app->user->getIdentity();
        if (CommonTrait::participantsImplode($user)) {
            $user->save();
        }

        Yii::$app->user->logout();
        if ($flash != null) {
            Yii::$app->session->setFlash(key($flash), current($flash));
        }

        return $this->goHome();
    }

//end actionLogout()

    /**
     * Prepare ActiveRecord object and renders user profile.
     */
    public function actionProfile() {
        $user = Yii::$app->user->identity;
        CommonTrait::setGlobalsStatus($user->status);


        return $this->render(
        'profile', ['model' => $user]
        );
    }

//end actionProfile()

    /**
     * Updates user profile.
     *
     * @return mixed
     */
    public function actionEditProfile() {
        $model = new EditProfileForm();
        if ($model->load(Yii::$app->request->post())) {
            $changesArray = $model->editProfile();

            if ($changesArray != null) {
                if ($changesArray['dataChanged']) {
                    if (!$changesArray['emailChanged'] and ! $changesArray['sexChanged']) {
                        Yii::$app->session->setFlash('success', 'Great! Your profile has been updated.');

                        return $this->redirect(['profile']);
                    } else if (!$changesArray['emailChanged'] and $changesArray['sexChanged']) {

                        return self::actionDeletePhoto();
                    }
                }

                return $this->redirect(['profile']);
            }
        }

        return $this->render(
        'editProfile', ['model' => $model]
        );
    }

//end actionEditProfile()

    /**
     * Mark user account for deletion.
     * 
     * @return mixed
     */
    public function actionDeleteAccount() {
        $model = new DeleteAccountForm();
        if ($model->load(Yii::$app->request->post()) && $model->deleteAccount()) {
            return self::actionLogout(['success' => 'Your account has been marked for deletion and will be deleted during next database cleaning procedure. Normally it happens monthly at the last days. Until that, you can restore your account by simply logging to the site.']);
        }

        return $this->render(
        'deleteAccount', ['model' => $model]
        );
    }
    
//end actionDeleteAccount()
    
    /**
     * Replace user photo in database with default image.
     * 
     * @return redirect /site/update-photo
     */
    public function actionDeletePhoto() {
        $user = Yii::$app->user->identity;
        CommonTrait::setBasePhoto($user);
        $user->save();

        return $this->redirect(['/site/update-photo']);
    }

//end actionDeletePhoto()

    /**
     * Updates user photo.
     *
     * @return mixed
     */
    public function actionUpdatePhoto() {
        $model = new UpdatePhotoForm();
        if ($model->load(Yii::$app->request->post()) and $model->editPhoto()) {
            Yii::$app->session->setFlash('success', 'Great! Your photo has been updated. In some browsers you need to refresh the page with cache cleaning (usually Ctrl-F5) to see all changes. ');

            return $this->refresh();
        }

        return $this->render(
        'updatePhoto', [
            'model' => $model,
            'userPhoto' => $model->user->photo
        ]
        );
    }

//end actionUpdatePhoto()



    /**
     * Renders user profile.
     * 
     * @return mixed
     */
    public function actionSettings() {
        $model = new SettingsForm();
        if ($model->load(Yii::$app->request->post()) && $model->updateSettings()) {
            return $this->refresh();
        }

        return $this->render(
        'settings', ['model' => $model]
        );
    }

//end actionSettings()

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup() {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                // if (Yii::$app->getUser()->login($user)) {
                if ($model->sendEmail($user)) {
                    $validateLink = Yii::$app->urlManager->createAbsoluteUrl(['site/resend-email', 'id' => $user->id, 'token' => $user->auth_key]);
                    Yii::$app->session->setFlash('success', "Great! Your account has been created. Check your email (including spam or junk folders) for further instructions. Don't close this window until you get the email because only here you can " . yii\helpers\Html::a('resend it.', $validateLink));

                    return $this->goHome();
                } else {
                    Yii::$app->session->setFlash('error', 'Oops... Something went wrong. There was an error during sending email. Try to sugnup again.');
                    $user->delete();
                    Yii::$app->session->destroy();
                }

                // }
            }
        }

        return $this->render(
        'signup', ['model' => $model]
        );
    }

//end actionSignup()

    /**
     * Resend confirmation email.
     *
     * @param $id - user id, $token - token used (auth_key) field {{user}} table
     *
     * @return conditional redirect
     */
    public function actionResendEmail($id, $token) {
        $user = User::findByAuthKey($id, $token);
        if ($user->new_email != null) {
            $user->email = $user->new_email;
        }

        if (SignupForm::sendEmail($user)) {
            $validateLink = Yii::$app->urlManager->createAbsoluteUrl(['site/resend-email', 'id' => $user->id, 'token' => $user->auth_key]);
            Yii::$app->session->setFlash('success', 'Confirmation email was resend. Check your email (including spam or junk folders) for further instructions. Or again ' . yii\helpers\Html::a('resend it.', $validateLink));
            if ($user->new_email != null) {
                return $this->redirect(['profile']);
            }
        } else if ($user->status = 5) {
            Yii::$app->session->setFlash('error', 'Oops... Something went wrong. There was an error during sending email. Try to signup again.');
            $user->delete();
            Yii::$app->session->destroy();

            return $this->redirect(['signup']);
        } else if ($user->status >= 10 and $user->new_email != null) {
            Yii::$app->session->setFlash('error', 'Oops... Something went wrong. There was an error during sending confirmation email. Your email will stay unchanged. Please try again later. If the mistake will remain - please contact us.');
            $user->email = $user->getOldAttribute('email');
            $user->new_email = null;
            $user->save();

            return $this->redirect(['profile']);
        }

        return $this->goHome();
    }

//end actionResendEmail()


    /*
     * Verify email confirmation token and if success set user status to ACTIVE
     *
     * @param $id - user id, $token - token (used 'auth_key' field 'user' table)
     * @return conditional redirect
     */

    public function actionEmailConfirmation($id, $token) {
        $user = User::findByAuthKey($id, $token);
        if (!$user) {
            Yii::$app->session->setFlash('error', 'Oops... Something went wrong. Unsuccessful email validation attempt. Please try again begins from our letter in your email.');

            return $this->goHome();
        } else {
            if (!$user->new_email and ( $user->status == 0 or $user->status > 5)) {
                Yii::$app->session->setFlash('warning', 'Your email already has been confirmed.');
            } else {
                Yii::$app->session->setFlash('success', 'Great! Your email confirmed!');
                if (!$user->new_email and $user->status == 5) {
                    $user->status = 10;
                    $user->save();
                } else {
                    $user->email = $user->new_email;
                    $user->new_email = null;
                    $user->save();
                }
            }

            return $this->redirect(['login']);
        }//end if
    }

//end actionEmailConfirmation()

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset() {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Password reset email was send and will be valid for an hour. Please check your email (including spam or junk folders) for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render(
        'requestPasswordResetToken', ['model' => $model]
        );
    }

//end actionRequestPasswordReset()

    /**
     * Resets password.
     *
     * @param string $token
     *
     * @return mixed
     *
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token) {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) and $model->resetPassword()) {
            return $this->goBack();
        }

        return $this->render(
        'resetPassword', ['model' => $model]
        );
    }

//end actionResetPassword()

    /**
     * Change user password.
     */
    public function actionChangePassword() {
        $model = new ChangePasswordForm();
        if ($model->load(Yii::$app->request->post()) and $model->changePassword()) {
            return $this->goBack();
        }

        return $this->render(
        'changePassword', ['model' => $model]
        );
    }

//end actionChangePassword()
}

//end class
