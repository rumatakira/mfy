<?php

/** @author Kirill Lapchinsky rumatakira74@gmail.com */

namespace frontend\controllers;

use Yii;
use common\models\User;
use common\traits\CommonTrait;
use frontend\models\ParticipantsForm;
use frontend\models\SearchParticipantsForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
// use yii\helpers\Url;

/**
 * ParticipantsController implements the CRUD actions for ParticipantsForm model.
 */
class ParticipantsController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

//end behaviors()

    /**
     * Lists all ParticipantsForm models.
     *
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new SearchParticipantsForm();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render(
        'index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]
        );
    }

//end actionIndex()

    /**
     * Displays a single ParticipantsForm model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id, $redirectFromUsersSearch = false) {
        $model = $this->findModel($id);
        CommonTrait::setGlobalsStatus($model->user->status);

        return $this->render(
        'view', [
            'model' => $model,
            'userPhoto' => $model->user->photo,
            'redirectFromUsersSearch' => $redirectFromUsersSearch,
        ]
        );
    }

//end actionView()

    /**
     * Creates a new ParticipantsForm model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate($id = null, $redirectFromUsersSearch = false) {
        $model = new ParticipantsForm();
        $model->scenario = ParticipantsForm::SCENARIO_CREATE;
        $model->userId = Yii::$app->user->getId();

        if ($id !== null) {
            $userToAdd = User::findIdentity($id);
            $model->participant = $userToAdd->username;
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('success', 'Your participant was successfully created.');

            return $this->redirect(['view', 'id' => $model->id, 'redirectFromUsersSearch' => $redirectFromUsersSearch]);
        } else {
            return $this->render(
            'create', ['model' => $model, 'redirectFromUsersSearch' => $redirectFromUsersSearch]
            );
        }
    }

//end actionCreate()

    /**
     * Updates an existing ParticipantsForm model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id, $redirectFromUsersSearch = false) {
        $model = $this->findModel($id);
        $model->scenario = ParticipantsForm::SCENARIO_UPDATE;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Your participant record was successfully updated.');
            return $this->redirect(['view', 'id' => $model->id, 'redirectFromUsersSearch' => $redirectFromUsersSearch]);
        } else {
            return $this->render(
            'update', ['model' => $model, 'redirectFromUsersSearch' => $redirectFromUsersSearch]
            );
        }
    }

//end actionUpdate()

    /**
     * Deletes an existing ParticipantsForm model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id, $redirectFromUsersSearch = false) {
        $this->findModel($id)->delete();
        if (!$redirectFromUsersSearch) {
            return $this->redirect(['index']);
        } else {
            return $this->redirect(['/search-users/index']);
        }
    }

//end actionDelete()

    /**
     * Finds the ParticipantsForm model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return ParticipantsForm the loaded model
     *
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = ParticipantsForm::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

//end findModel()
}

//end class
