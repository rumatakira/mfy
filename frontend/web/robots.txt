User-agent: *
Disallow:
Disallow: /img/
Disallow: /cgi-bin/
Disallow: /tmp/
Disallow: /assets/
Disallow: /css/
Disallow: /forum/
Disallow: /bugs/
Disallow: /backend/