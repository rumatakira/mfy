<?php

/** @author Kirill Lapchinsky rumatakira74@gmail.com */

namespace frontend\models;

use Yii;

/**
 * Password reset form.
 */
class ChangePasswordForm extends SignupForm {

    /**
     * @var \common\models\User
     */
    protected
    $user;

    public
    function __construct () {
        $this->setScenario('change-password');
        $this->user = Yii::$app->user->getIdentity();
    }

//end __construct()

    /**
     * Change user password.
     *
     * @return bool if password was reset.
     */
    public
    function changePassword () {
        if ($this->validate()) {
            // !!! uncomment in production !!! 
            // $this->user->setPassword($this->newPassword);
            if ($this->user->save()) {
                Yii::$app->session->setFlash('success', 'New password was saved.');

                return true;
            } else {
                Yii::$app->session->setFlash('error', 'Oops... Something went wrong. There was an error during database operation. Try to logout and login again. If the mistake will remain - please contact us.');
            }
        }

        return;
    }

//end changePassword()
}

//end class
