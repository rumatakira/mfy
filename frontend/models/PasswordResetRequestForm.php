<?php

/** @author Kirill Lapchinsky rumatakira74@gmail.com */

namespace frontend\models;

use common\models\User;
use yii\base\Model;

/**
 * Password reset request form.
 */
class PasswordResetRequestForm extends Model {

    public
    $email;

    /**
     * {@inheritdoc}
     */
    public static
    function tableName () {
        return 'user';
    }

//end tableName()

    /**
     * {@inheritdoc}
     */
    public
    function rules () {
        return [
            [
                'email', 'filter', 'filter' => 'trim'
            ],
            [
                'email',
                'required',
            ],
            [
                'email',
                'email',
            ],
            ['email', 'exist',
                'targetClass' => '\common\models\User',
                'filter' => [
                    'status' => [
                        User::STATUS_IDENTITYVERIFIED,
                        User::STATUS_PHOTOVERIFIED,
                        User::STATUS_ACTIVE,
                        User::STATUS_DELETED,
                    ],
                ],
                'message' => 'There is no user with such email.',
            ],
        ];
    }

//end rules()

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public
    function sendEmail () {
        // @var $user User
        $user = User::findByEmail($this->email);
        if ($user) {
            if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
                $user->generatePasswordResetToken();
            }

            if ($user->save()) {
                return \Yii::$app->mailer->compose(['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'], ['user' => $user])->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])->setTo($this->email)->setSubject('Password reset for ' . \Yii::$app->name)->send();
            }
        }

        return false;
    }

//end sendEmail()
}

//end class
