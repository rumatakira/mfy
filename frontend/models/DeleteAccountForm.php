<?php

/** @author Kirill Lapchinsky rumatakira74@gmail.com */

namespace frontend\models;

use common\models\User;
use Yii;

/**
 * Mark user account for deletion.
 */
class DeleteAccountForm extends SignupForm {

    public $user;

    public function __construct () {
        $this->setScenario('delete-account');
        $this->user = Yii::$app->user->identity;
        $this->secretQuestion = $this->user->secret_question;
    }

//end __construct()

    /**
     * Set user status = 0 (mark for deletion).
     *
     * @return true|null for saved model or null if deletion fails
     */
    public function deleteAccount () {
        if ($this->validate()) {
            $this->user->status = 0;
            if ($this->user->save()) {

                return true;
            } else {
                Yii::$app->session->setFlash('error', 'Oops... Something went wrong. There was an error during database operation. Try to logout and login again. If the mistake will remain - please contact us.');
            }
        }

        return;
    }

//end deleteAccount()
}

//end class
