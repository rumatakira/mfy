<?php

/** @author Kirill Lapchinsky rumatakira74@gmail.com */

namespace frontend\models;

use common\models\User;
use yii\base\InvalidParamException;
use Yii;

/**
 * Password reset form.
 */
class ResetPasswordForm extends SignupForm {

    /**
     * @var \common\models\User
     */
    protected
    $user;

    /**
     * Creates a form model given a token.
     *
     * @param string $token
     * @param array  $config name-value pairs that will be used to initialize the object properties
     *
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public
    function __construct ($token) {
        $this->setScenario('reset-password');
        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('Password reset token cannot be blank.');
        }

        $this->user = User::findByPasswordResetToken($token);
        if (!$this->user) {
            throw new InvalidParamException('Wrong password reset token.');
        }
    }

//end __construct()

    /**
     * Resets password.
     *
     * @return bool if password was reset.
     */
    public
    function resetPassword () {
        if ($this->validate()) {
            $this->user->setPassword($this->password);
            $this->user->removePasswordResetToken();

            if ($this->user->save()) {
                Yii::$app->session->setFlash('success', 'New password was saved.');

                return true;
            } else {
                Yii::$app->session->setFlash('error', 'Oops... Something went wrong. There was an error during database operation. Try to logout and login again. If the mistake will remain - please contact us.');
            }
        }

        return;
    }

//end resetPassword()
}

//end class
