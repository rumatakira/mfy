<?php

/** @author Kirill Lapchinsky rumatakira74@gmail.com */

namespace frontend\models;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table {{participants}}.
 *
 * @property string $participant
 * @property string $category
 */
class ParticipantsForm extends ActiveRecord {

    const
    SCENARIO_CREATE = 'create';
    const
    SCENARIO_UPDATE = 'update';

    public
    $currentUser;

    public
    function __construct () {
        $this->currentUser = Yii::$app->user->identity;
    }

//end __construct()

    /**
     * {@inheritdoc}
     */
    public static
    function tableName () {
        return 'participants';
    }

//end tableName()

    public
    function getUser () {
        return $this->hasOne(\common\models\User::className(), ['username' => 'participant']);
    }

//end getUser()

    public
    function scenarios () {
        return [
            self::SCENARIO_CREATE => [
                'participant',
                'category',
            ],
            self::SCENARIO_UPDATE => ['category'],
        ];
    }

//end scenarios()

    /**
     * {@inheritdoc}
     */
    public
    function rules () {
        return [
            [
                'participant',
                'required',
            ],
            [
                'participant', 'string', 'max' => 32
            ],
            [
                'category', 'string', 'max' => 255
            ],
            [
                'category', 'default', 'value' => 'uncategorized'
            ],
            [
                [
                    'participant',
                    'category',
                ], 'filter', 'filter' => 'trim'
            ],
            ['category', 'filter', 'filter' => function () {
                    // begins all words with Up letter
                    return ucwords($this->category);
                }
            ],
            [
                'participant', 'exist', 'targetAttribute' => 'username', 'targetClass' => '\common\models\User', 'message' => 'There is no such user in our database.'
            ],
            [
                'participant', 'unique', 'filter' => ['userId' => $this->userId], 'message' => 'Such participant already exists. You can only change category, or delete this participant.'
            ],
            [
                'participant', 'compare', 'compareValue' => $this->currentUser->username, 'operator' => '!=', 'message' => "You can't add yourself to participants.", 'on' => self::SCENARIO_CREATE
            ],
        ];
    }

//end rules()

    /**
     * {@inheritdoc}
     */
    public
    function attributeLabels () {
        return [
            'participant' => 'Participant',
            'category' => 'Category',
            'user.first_name' => 'First Name',
            'user.last_name' => 'Last Name',
            'user.country' => 'Country',
            'user.area' => 'Area',
            'user.city' => 'City',
            'user.photo' => 'User photo',
        ];
    }

//end attributeLabels()
}

//end class
