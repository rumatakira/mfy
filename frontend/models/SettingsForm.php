<?php

/** @author Kirill Lapchinsky rumatakira74@gmail.com */

namespace frontend\models;

use common\models\User;
use yii\db\ActiveRecord;
use Yii;

/**
 * Update user notification settings.
 */
class SettingsForm extends ActiveRecord {

    public $notifications;
    public $notificationsFrom;
    public $showEmail;
    protected $user;

    /**
     * {@inheritdoc}
     */
    public static function tableName () {
        return 'user';
    }

//end tableName()

    public function __construct () {
        $this->user = Yii::$app->user->identity;
        $this->notifications = $this->user->notifications;
        $this->notificationsFrom = $this->user->notifications_from;
        $this->showEmail = $this->user->show_email;
    }

//end __construct()

    public function rules () {
        return [
            [
                [
                    'notifications',
                    'notificationsFrom',
                    'showEmail',
                ], 'safe',
            ],
        ];
    }

//end rules()

    /**
     * Update email notification settings.
     *
     * @return true|null for saved model or null if fails
     */
    public function updateSettings () {
        $this->user->notifications = $this->notifications;
        $this->user->notifications_from = $this->notificationsFrom;
        $this->user->show_email = $this->showEmail;
        if ($this->user->save()) {
            Yii::$app->session->addFlash('success', 'Your notifications and messages settings updated.');

            return true;
        } else {
            Yii::$app->session->setFlash('error', 'Oops... Something went wrong. There was an error during database operation. Try to logout and login again. If the mistake will remain - please contact us.');
        }

        return;
    }

//end updateSettings()
}

//end class
