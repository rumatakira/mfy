<?php

/** @author Kirill Lapchinsky rumatakira74@gmail.com */

namespace frontend\models;

use Yii;

/**
 * Update user profile form.
 */
class EditProfileForm extends SignupForm {

    protected $user;

    public
    function __construct () {
        $this->setScenario('edit-profile');
        $this->user = Yii::$app->user->identity;
        $this->username = $this->user->username;
        $this->email = $this->user->email;
        $this->firstName = $this->user->first_name;
        $this->lastName = $this->user->last_name;
        $this->sex = $this->user->sex;
        $this->birthDate = $this->user->birth_date;
        $this->country = $this->user->country;
        $this->area = $this->user->area;
        $this->city = $this->user->city;
    }

//end __construct()

    public
    function attributeLabels () {
        return ['photo' => 'Current photo'];
    }

//end attributeLabels()

    /**
     * Update user profile.
     *
     * @return array $returnArray bool - 'emailChanged', 'sexChanged'.
     */
    public
    function editProfile () {
        if ($this->validate()) {
            $returnArray['emailChanged'] = false;
            if ($this->user->sex == $this->sex) {
                $returnArray['sexChanged'] = false;
            } else {
                $returnArray['sexChanged'] = true;
            }

            $this->user->username = $this->username;
            $this->user->first_name = $this->firstName;
            $this->user->last_name = $this->lastName;
            $this->user->sex = $this->sex;
            $this->user->birth_date = $this->birthDate;
            $this->user->country = $this->country;
            $this->user->area = $this->area;
            $this->user->city = $this->city;
            if ($this->user->email != $this->email) {
                $this->user->email = $this->email;
                if ($this->sendEmail($this->user)) {
                    $validateLink = Yii::$app->urlManager->createAbsoluteUrl(['site/resend-email', 'id' => $this->user->id, 'token' => $this->user->auth_key]);
                    Yii::$app->session->setFlash('success', "Your account has been updated. Check your email (including spam or junk folders) for further instructions. Don't close this window until you get the email because only here you can " . yii\helpers\Html::a('resend it.', $validateLink));
                    $this->user->new_email = $this->email;
                    $returnArray['emailChanged'] = true;
                } else {
                    Yii::$app->session->setFlash('error', 'Oops... Something went wrong. There was an error during sending confirmation email. Your email will stay unchanged. Please try again later. If the mistake will remain - please contact us.');
                }

                $this->user->email = $this->user->getOldAttribute('email');
            }

            $returnArray['dataChanged'] = false;
            foreach ($this->user as $name => $value) {
                if ($this->user->isAttributeChanged($name)) {
                    $returnArray['dataChanged'] = true;
                }
            }

            if ($this->user->save()) {
                return $returnArray;
            } else {
                Yii::$app->session->setFlash('error', 'Oops... Something went wrong. There was server error during saving your profile to database. Please try again later. If the mistake will remain - please contact us.');
            }
        }//end if

        return;
    }

//end editProfile()
}

//end class
