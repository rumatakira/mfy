<?php

/** @author Kirill Lapchinsky rumatakira74@gmail.com */

namespace frontend\models;

use common\models\User;
use yii\db\ActiveRecord;
use common\traits\CommonTrait;

/*
 * Signup form - model behind user sign up.
 */
class SignupForm extends ActiveRecord {

    public $username;
    public $email;
    public $password;
    public $newPassword;
    public $passwordCheck;
    public $firstName;
    public $lastName;
    public $sex;
    public $birthDate;
    public $country;
    public $area;
    public $city;
    public $secretQuestion;
    public $secretQuestionAnswer;
    public $verificationCodeCaptcha;
    public $photoFile;

    const SCENARIO_SIGNUP = 'signup';
    const SCENARIO_PROFILE = 'edit-profile';
    const SCENARIO_DELETE_ACCOUNT = 'delete-account';
    const SCENARIO_UPDATE_PHOTO = 'update-photo';
    const SCENARIO_RESET_PASSWORD = 'reset-password';
    const SCENARIO_CHANGE_PASSWORD = 'change-password';

    /**
     * {@inheritdoc}
     */
    public static function tableName () {
        return 'user';
    }

//end tableName()

    public function __construct () {
        $this->setScenario('signup');
    }

//end __construct()

    public function scenarios () {
        return [
            self::SCENARIO_SIGNUP => [
                'username',
                'email',
                'password',
                'passwordCheck',
                'firstName',
                'lastName',
                'sex',
                'birthDate',
                'country',
                'area',
                'city',
                'secretQuestion',
                'secretQuestionAnswer',
                'verificationCodeCaptcha',
            ],
            self::SCENARIO_PROFILE => [
                'username',
                'email',
                'new_email',
                'password',
                'firstName',
                'lastName',
                'sex',
                'birthDate',
                'country',
                'area',
                'city',
                'photoFile',
            ],
            self::SCENARIO_DELETE_ACCOUNT => [
                'password',
                'secretQuestionAnswer',
                'verificationCodeCaptcha',
            ],
            self::SCENARIO_UPDATE_PHOTO => ['photoFile'],
            self::SCENARIO_RESET_PASSWORD => [
                'password',
                'passwordCheck',
                'verificationCodeCaptcha',
            ],
            self::SCENARIO_CHANGE_PASSWORD => [
                'newPassword',
                'password',
                'passwordCheck',
                'verificationCodeCaptcha',
            ],
        ];
    }

//end scenarios()

    /**
     * {@inheritdoc}
     */
    public function rules () {
        return [
            [
                'username', 'filter', 'filter' => 'trim'
            ],
            [
                'username',
                'required',
            ],
            [
                'username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.', 'on' => self::SCENARIO_SIGNUP
            ],
            ['username', 'unique', 'when' => function ($model, $attribute) {
                    return $this->$attribute != $this->user->$attribute;
                }, 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.', 'on' => self::SCENARIO_PROFILE
            ],
            [
                'username', 'string', 'min' => 3, 'max' => 32
            ],
            [
                'username', 'match', 'pattern' => '/^[a-zA-Z0-9._-]*$/i'
            ],
            [
                'email', 'filter', 'filter' => 'trim'
            ],
            [
                'email',
                'required',
            ],
            [
                'email',
                'email',
            ],
            [
                'email', 'string', 'max' => 128
            ],
            [
                'email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.', 'on' => self::SCENARIO_SIGNUP
            ],
            ['email', 'unique', 'when' => function ($model, $attribute) {
                    return $this->$attribute != $this->user->$attribute;
                }, 'targetClass' => '\common\models\User', 'message' => 'This email has already been taken.', 'on' => self::SCENARIO_PROFILE
            ],
            [
                [
                    'password',
                    'newPassword',
                    'passwordCheck',
                ], 'required',
            ],
            [
                [
                    'password',
                    'newPassword',
                ], 'string', 'min' => 8
            ],
            [
                [
                    'password',
                    'newPassword',
                ], 'match', 'pattern' => '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/', 'message' => 'Pssword pattern is invalid.'
            ],
            // [['password', 'newPassword'], 'match', 'pattern' => '/^(?=(?:.*[a-z]){2})(?=(?:.*[A-Z]){2})(?=(?:.*\d){2})(?=(?:.*[!@#$%^&*-]){2}).{8,}$/', 'message' => 'Pssword pattern is invalid.'],
            [
                'newPassword',
                'doesPasswordChanged',
            ],
            [
                'password', 'validateUserPassword', 'on' => [
                    self::SCENARIO_PROFILE,
                    self::SCENARIO_DELETE_ACCOUNT,
                    self::SCENARIO_CHANGE_PASSWORD,
                ]
            ],
            [
                'passwordCheck', 'compare', 'compareAttribute' => 'password', 'message' => "Passwords don't match!", 'on' => [
                    self::SCENARIO_SIGNUP,
                    self::SCENARIO_RESET_PASSWORD,
                ]
            ],
            [
                'passwordCheck', 'compare', 'compareAttribute' => 'newPassword', 'message' => "Passwords don't match!", 'on' => self::SCENARIO_CHANGE_PASSWORD
            ],
            [
                'firstName', 'filter', 'filter' => 'trim'
            ],
            [
                'firstName',
                'required',
            ],
            [
                'firstName', 'string', 'max' => 32
            ],
            ['firstName', 'filter', 'filter' => function () {
                    // begins all words with Up letter
                    return ucwords($this->firstName);
                }
            ],
            [
                'lastName', 'filter', 'filter' => 'trim'
            ],
            [
                'lastName',
                'required',
            ],
            [
                'lastName', 'string', 'max' => 32
            ],
            ['lastName', 'filter', 'filter' => function () {
                    // begins all words with Up letter
                    return ucwords($this->lastName);
                }
            ],
            [
                'sex',
                'required',
            ],
            [
                'birthDate', 'date', 'format' => 'yyyy-MM-dd'
            ],
            [
                'birthDate',
                'required',
            ],
            [
                'birthDate',
                'validateBirthDate',
            ],
            [
                'country',
                'safe',
            ],
            [
                'area',
                'required',
            ],
            [
                'area', 'string', 'max' => 32
            ],
            [
                'area', 'filter', 'filter' => 'trim'
            ],
            ['area', 'filter', 'filter' => function () {
                    // begins all words with Up letter
                    return ucwords($this->area);
                }
            ],
            [
                'city',
                'required',
            ],
            [
                'city', 'filter', 'filter' => 'trim'
            ],
            [
                'city', 'string', 'max' => 32
            ],
            ['city', 'filter', 'filter' => function () {
                    // begins all words with Up letter
                    return ucwords($this->city);
                }
            ],
            [
                'secretQuestion',
                'required',
            ],
            [
                'secretQuestionAnswer',
                'required',
            ],
            [
                'secretQuestionAnswer', 'filter', 'filter' => 'trim'
            ],
            [
                'secretQuestionAnswer', 'string', 'max' => 255
            ],
            [
                'secretQuestionAnswer', 'validateSecretAnswer', 'on' => self::SCENARIO_DELETE_ACCOUNT
            ],
            [
                'verificationCodeCaptcha',
                'captcha',
            ],
            [
                'photoFile', 'image', 'extensions' => 'jpg, jpeg, png', 'wrongExtension' => 'Only jpg, jpeg, png file types are allowed.', 'mimeTypes' => 'image/jpg, image/jpeg, image/png', 'wrongMimeType' => 'Wrong MIME type of the file. Only jpg, jpeg, png file types are allowed. ', 'minWidth' => 550, 'message' => 'Oops... Something went wrong. There was an error during uploading file. Please try again later. If the mistake will remain - please contact us.'
            ],
        ];
    }

//end rules()

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup () {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->first_name = $this->firstName;
            $user->last_name = $this->lastName;
            $user->sex = $this->sex;
            $user->birth_date = $this->birthDate;
            $user->country = $this->country;
            $user->area = $this->area;
            $user->city = $this->city;
            $user->secret_question = $this->secretQuestion;
            $user->setSecretQuestionAnswer($this->secretQuestionAnswer);
            $user->generateAuthKey();
            CommonTrait::setBasePhoto($user);
            if ($user->save()) {
                return $user;
            }
        }

        return;
    }

//end signup()


    /*
     * Validate if user age is in the range 16-100 y.o.
     */

    public function validateBirthDate ($attribute, $params) {
        $date = new \DateTime();
        date_sub($date, date_interval_create_from_date_string('16 years'));
        $minAgeDate = date_format($date, 'Y-m-d');
        date_sub($date, date_interval_create_from_date_string('100 years'));
        $maxAgeDate = date_format($date, 'Y-m-d');
        if ($this->$attribute > $minAgeDate) {
            $this->addError($attribute, 'Date is too small.');
        } elseif ($this->$attribute < $maxAgeDate) {
            $this->addError($attribute, 'Date is to big.');
        }
    }

//end validateBirthDate()


    /*
     * Validate if user input the old passwords as new one.
     */

    public function doesPasswordChanged ($attribute, $param) {
        if ($this->user->validatePassword($this->newPassword)) {
            $this->addError($attribute, 'Password do not changed!');
        }
    }

//end doesPasswordChanged()

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array  $params    the additional name-value pairs given in the rule
     */
    public function validateUserPassword ($attribute, $params) {
        if (!$this->hasErrors()) {
            if (!$this->user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect password.');
            }
        }
    }

//end validateUserPassword()

    /**
     * Validates the secret question answer.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array  $params    the additional name-value pairs given in the rule
     */
    public function validateSecretAnswer ($attribute, $params) {
        if (!$this->hasErrors()) {
            if (!$this->user->validateSecretQuestionAnswer($this->secretQuestionAnswer)) {
                $this->addError($attribute, 'Incorrect answer.');
            }
        }
    }

//end validateSecretAnswer()


    /*
     * Send validation email.
     */

    public static function sendEmail ($user) {
        return \Yii::$app->mailer->compose(['html' => 'emailConfirmationToken-html', 'text' => 'emailConfirmationToken-text'], ['user' => $user])
        // ->setFrom(['support@mafiaonlinegame.com' => \Yii::$app->name . ' robot'])
        ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])->setTo($user->email)->setSubject('Email validation for ' . \Yii::$app->name)->send();
    }

//end sendEmail()
}

//end class
