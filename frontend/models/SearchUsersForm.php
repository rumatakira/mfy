<?php

/** @author Kirill Lapchinsky rumatakira74@gmail.com */

namespace frontend\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\models\User;

/**
 * SearchUsersForm represents user search form.
 */
class SearchUsersForm extends User {

    public
    function attributeLabels () {
        return [
            'username' => 'Username',
            'email' => 'Email',
            'created_at' => 'Created at',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'sex' => 'Sex',
            'birth_date' => 'Birth Date',
            'country' => 'Country',
            'area' => 'Area',
            'city' => 'City',
            'photo' => 'User photo',
        ];
    }

//end attributeLabels()

    public
    function attributes () {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['username', 'email', 'first_name', 'last_name', 'sex', 'birth_date', 'country', 'area', 'city']);
    }

//end attributes()

    /**
     * {@inheritdoc}
     */
    public
    function rules () {
        return [
            [
                [
                    'username',
                    'first_name',
                    'last_name',
                    'sex',
                    'birth_date',
                    'country',
                    'area',
                    'city',
                ], 'safe',
            ],
            [
                'email', 'email', 'skipOnEmpty' => true
            ],
            [
                'email', 'filter', 'filter' => 'trim', 'skipOnEmpty' => true
            ],
            [
                'email', 'string', 'max' => 128, 'skipOnEmpty' => true
            ],
            [
                'email', 'doesShowEmail', 'skipOnEmpty' => true
            ],
        ];
    }

//end rules()

    /**
     * Validation rule function
     */
    public
    function doesShowEmail ($attribute, $params) {
        $user = User::findByEmail($this->email);
        if ($user != null and $user->show_email == 1) {
            return $this->email;
        } else {
            $this->addError($attribute, 'There is no such user or email was hid by the user.');
        }
    }

//end doesShowEmail()

    /**
     * Creates data provider instance with search query applied.
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public
    function search ($params, $additionalParams) {

        $id = Yii::$app->user->getId();

        $participantsArray = (new \yii\db\Query())
        ->select('participant')
        ->from('participants')
        ->where(['userId' => $id])
        ->column();

        /*
         * alternativelly can be used direct SQL command
         * 
         * $participantsArray = Yii::$app->db->createCommand("SELECT [[participant]] FROM {{participants}} WHERE [[userId]] = :id")
          ->bindValue(':id', $id)
          ->queryColumn();
         */
        if ($additionalParams['viewOnlyPremium'] !== null) {
            $query = $this->find()->where("id != $id")->andWhere('status = 33')->andWhere(['not in', 'username', $participantsArray]);
        } elseif ($additionalParams['viewOnlyApprovedPhoto'] !== null) {
            $query = $this->find()->where("id != $id")->andWhere('photo_verified = 1')->andWhere(['not in', 'username', $participantsArray]);
        } elseif ($additionalParams['viewOnlyWithPhoto'] !== null) {
            $query = $this->find()->where("id != $id")->andWhere('photo_verified <> -1')->andWhere(['not in', 'username', $participantsArray]);
        } else {
            $query = $this->find()->where("id != $id")->andWhere(['not in', 'username', $participantsArray]);
        }

        $dataProvider = new ActiveDataProvider(
        [
            'query' => $query,
            'pagination' => ['pageSize' => 15],
            'sort' => [
                'attributes' => [
                    'username',
                    'created_at',
                    'first_name',
                    'last_name',
                    'sex',
                    'birth_date',
                    'country',
                    'area',
                    'city',
                ],
                'defaultOrder' => ['username' => SORT_ASC],
            ],
        ]
        );

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query
        ->andFilterWhere(['like', 'username', $this->username])
        ->andFilterWhere(['like', 'email', $this->email])
        ->andFilterWhere(['like', 'first_name', $this->first_name])
        ->andFilterWhere(['like', 'last_name', $this->last_name])
        ->andFilterWhere(['like', 'sex', $this->sex])
        ->andFilterWhere(['like', 'birth_date', $this->birth_date])
        ->andFilterWhere(['like', 'country', $this->country])
        ->andFilterWhere(['like', 'area', $this->area])
        ->andFilterWhere(['like', 'city', $this->city]);

        return $dataProvider;
    }

//end search()
}

//end class
