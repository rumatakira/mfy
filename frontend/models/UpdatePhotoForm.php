<?php

/** @author Kirill Lapchinsky rumatakira74@gmail.com */

namespace frontend\models;

use yii\web\UploadedFile;
use common\traits\CommonTrait;
use yii\imagine\Image;
use Yii;

/**
 * Update user photo.
 */
class UpdatePhotoForm extends SignupForm {

    public
    $user;

    public
    function __construct () {
        $this->user = Yii::$app->user->identity;
        $this->setScenario('update-photo');
    }

//end __construct()

    public
    function attributeLabels () {
        return ['photo' => 'Current photo'];
    }

//end attributeLabels()

    /**
     * Update user profile.
     *
     * @return string $flagEmailChanged|null the saved model or null if saving fails
     */
    public
    function editPhoto () {
        if ($this->validate()) {
            $id = Yii::$app->user->getId();
            $this->photoFile = UploadedFile::getInstance($this, 'photoFile');
            if (!$this->photoFile->hasError) {
                $this->user->photo_extension = $this->photoFile->extension;
                try {
                    $this->photoFile->saveAs($_SERVER['DOCUMENT_ROOT'] . '/tmp/' . "$id" . '.' . $this->photoFile->extension);
                    Image::thumbnail($_SERVER['DOCUMENT_ROOT'] . '/tmp/' . "$id" . '.' . $this->photoFile->extension, $width = 550, $height = null)->save($_SERVER['DOCUMENT_ROOT'] . '/tmp/' . "$id" . '.' . $this->photoFile->extension);
                    CommonTrait::setPhotoBorder($this->user);
                    Image::watermark($_SERVER['DOCUMENT_ROOT'] . '/tmp/' . "$id" . '.' . $this->photoFile->extension, $_SERVER['DOCUMENT_ROOT'] . '/img/watermark.png', [20, 20])->save($_SERVER['DOCUMENT_ROOT'] . '/tmp/' . "$id" . '.' . $this->photoFile->extension);
                } catch (ErrorException $e) {
                    Yii::$app->session->setFlash('error', 'Oops... Something went wrong. There was server error during saving your photo. Try again later. If the mistake will remain - please contact us.');

                    return false;
                }
                $this->user->photo_verified = 0; //photo not verified
                $this->user->status = 10;
                CommonTrait::prepareImageForDatabase($this->user);
                try {
                    $this->user->save();
                    try {
                        unlink($_SERVER['DOCUMENT_ROOT'] . '/tmp/' . "$id" . '.' . $this->photoFile->extension);
                    } catch (ErrorException $e) {
                        
                    }

                    return true;
                } catch (ErrorException $e) {
                    Yii::$app->session->setFlash('error', 'Oops... Something went wrong. There was server error during saving your photo to database. Try to logout and login again. If the mistake will remain - please contact us.');

                    return false;
                }
            } else {
                Yii::$app->session->setFlash('error', 'Oops... Something went wrong. There was an error during uploading file. Please try again later. If the mistake will remain - please contact us.');
            }//end if
        }//end if

        return false;
    }

//end editPhoto()
}

//end class
