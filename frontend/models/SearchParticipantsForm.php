<?php

/** @author Kirill Lapchinsky rumatakira74@gmail.com */

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SearchParticipantsForm represents the model behind the search form about `frontend\models\ParticipantsForm`.
 */
class SearchParticipantsForm extends ParticipantsForm {

    /**
     * {@inheritdoc}
     */
    public
    function scenarios () {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

//end scenarios()

    public
    function attributes () {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['user.first_name', 'user.last_name', 'user.country', 'user.area', 'user.city']);
    }

//end attributes()

    /**
     * {@inheritdoc}
     */
    public
    function rules () {
        return [
            [
                [
                    'id',
                    'userId',
                    'participant',
                    'category',
                    'user.first_name',
                    'user.last_name',
                    'user.country',
                    'user.area',
                    'user.city',
                ], 'safe',
            ],
        ];
    }

//end rules()

    /**
     * Creates data provider instance with search query applied.
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public
    function search ($params) {

        $query = ParticipantsForm::find()->joinWith('user')->where(['userId' => $this->currentUser->id]);

        $dataProvider = new ActiveDataProvider(
        [
            'query' => $query,
            'pagination' => ['pageSize' => 15],
            'sort' => [
                'attributes' => [
                    'participant',
                    'category',
                    'user.first_name',
                    'user.last_name',
                    'user.country',
                    'user.area',
                    'user.city',
                ],
                'defaultOrder' => ['participant' => SORT_ASC],
            ],
        ]
        );

        $this->load($params);

        $query
        ->andFilterWhere(['like', 'participant', $this->participant])
        ->andFilterWhere(['like', 'category', $this->category])
        ->andFilterWhere(['like', 'user.first_name', $this->getAttribute('user.first_name')])
        ->andFilterWhere(['like', 'user.last_name', $this->getAttribute('user.last_name')])
        ->andFilterWhere(['like', 'user.country', $this->getAttribute('user.country')])
        ->andFilterWhere(['like', 'user.area', $this->getAttribute('user.area')])
        ->andFilterWhere(['like', 'user.city', $this->getAttribute('user.city')]);

        return $dataProvider;
    }

//end search()
}

//end class
