<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="wrap">
            <?php
            if (Yii::$app->user->isGuest) {
                NavBar::begin([
                    'brandLabel' => Html::img('@web/img/main_logo.png', ['class' => 'img-responsive', 'style' => 'vertical-align: baseline; width: auto; height: 50px; padding: 0px; margin-top: -15px;']),
                    'brandUrl' => Yii::$app->homeUrl,
                    'options' => [
                        'class' => 'navbar-default navbar-fixed-top',
                    ],
                ]);
            } else {
                $user = Yii::$app->user->getIdentity();
                NavBar::begin([
                    'brandLabel' => Html::img("data:image/png;base64, $user->photo", ['class' => 'img-responsive', 'style' => 'vertical-align: baseline; width: auto; height: 50px; padding: 0px; margin-top: -15px;']),
                    'brandUrl' => ['/site/profile'],
                    'options' => [
                        'class' => 'navbar-default navbar-fixed-top',
                    ],
                ]);
            }
            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => 'Play the game', 'url' => ['/site/signup']];
            } else {
                $menuItems[] = ['label' => 'Play the game', 'url' => '@mafiaGamePlay'];
            }
            $menuItems[] = ['label' => 'Home', 'url' => ['/site/index']];
            $menuItems[] = [
                'label' => 'About',
                'items' => [
                    [
                        'label' => 'Our mission',
                        'url' => '@mafiaSiteMission',
                        'linkOptions' => ['target' => '_blank'],
                    ],
                    [
                        'label' => 'About us',
                        'url' => '@mafiaSiteAbout',
                        'linkOptions' => ['target' => '_blank'],
                    ],
                    [
                        'label' => 'Site map',
                        'url' => ['/site/site-map'],
                    ],
                ],
            ];
            $menuItems[] = [
                'label' => 'Rules',
                'items' => [
                    [
                        'label' => 'Site rules',
                        'url' => '@mafiaSiteRules',
                        'linkOptions' => ['target' => '_blank'],
                    ],
                    [
                        'label' => 'Game rules',
                        'url' => '@mafiaGameRules',
                        'linkOptions' => ['target' => '_blank'],
                    ],
                ],
            ];
            if (Yii::$app->user->isGuest) {
                $menuItems[] = [
                    'label' => 'Contact us',
                    'items' => [
                        [
                            'label' => 'Guest contact form',
                            'url' => ['/site/contact'],
                        ],
                    ],
                ];
            } else {
                $menuItems[] = [
                    'label' => 'Contact us',
                    'items' => [
                        [
                            'label' => 'Contact us',
                            'url' => '@mafiaSiteForum',
                            'linkOptions' => ['target' => '_blank'],
                        ],
                        [
                            'label' => 'Report a bug',
                            'url' => '@mafiaSiteBugs',
                            'linkOptions' => ['target' => '_blank'],
                        ],
                    ],
                ];
            }
            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']]; //, 'visible' => Yii::$app->user->isGuest];
                $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']]; //, 'visible' => Yii::$app->user->isGuest];
            } else {
                $menuItems[] = [
                    'label' => 'Account',
                    'items' => [
                        [
                            'label' => 'Profile',
                            'url' => ['/site/profile'],
                        ],
                        [
                            'label' => 'Settings',
                            'url' => ['/site/settings'],
                        ],
                        [
                            'label' => 'Participants',
                            'url' => ['/participants/index'],
                        ],
                        [
                            'label' => 'Seach users',
                            'url' => ['/search-users/index'],
                        ],
                        [
                            'label' => 'Messages',
                            'url' => ['/site/messages'],
                        ],
                        [
                            'label' => 'Forum',
                            'url' => '@mafiaSiteForum',
                            'linkOptions' => ['target' => '_blank'],
                        ],
                        '<li class="divider"></li>',
                        [
                            'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                            'url' => ['/site/logout'],
                            'linkOptions' => ['data-method' => 'post'],
                        ],
                    ],
                ];
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
            ]);

            NavBar::end();
            ?>

            <div class="container">
                <?=
                Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ])
                ?>
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <p class="pull-left">&copy;&nbsp;<?= date('Y') ?>&nbsp;&nbsp;
                    <a href="https://bitbucket.org/rumatakira/">Kiryl Lapchynski</a> (full stack),&nbsp;
                    <a href="https://www.facebook.com/ksenia.sorokina.7927">Ksenia Sorokina</a>&nbsp;(design),&nbsp;
                    <a href="https://www.facebook.com/profile.php?id=100001552140549">Alena Kuryanovich</a>&nbsp;(face of the game).</p>
                <p class="pull-right" align="middle">
                    <a href="https://twitter.com/mafiaonlinegame" title="We at Twitter" target='_blank'>
                        <img src="/img/twitter.png" align="middle" alt="We at Twitter" height="32" width="32" ></a>
                    <!-- <a href="https://www.facebook.com/pg/mafiaonlinegame/posts" title="We at Facebook" target='_blank'>
                        <img src="/img/facebook.png" align="middle" alt="We at Facebook" height="32" width="32" ></a> -->
                    &nbsp;&nbsp;&nbsp;&nbsp;<?= Yii::powered() ?>.</p>
            </div>
        </footer>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
