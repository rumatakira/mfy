<?php
/* @var $this yii\web\View */

use yii\bootstrap\Html;

$this->title = 'Mafia game';
?>

<div align=center>
    <p>
        This is demo version of the site. The project is under construction, so some links and functions may not work correctly. You can signup with your real email or login using usernames: 	jullrich, ada.wintheiser, ablanda and password: Demo2016
    </p>
</div>
<div class="site-index">

    <div class="body-content">
        <div class="jumbotron">
            <h1>"Mafia" online role game</h1>

            <p class="lead">the best way to spend free time, to communicate and to earn some money!</p>

            <p>
                <?php
                if (Yii::$app->user->isGuest) {
                    echo Html::a('Get started!', ['signup'], ['class' => 'btn btn-main-red']);
                } else {
                    echo Html::a('Get started!', '@mafiaGamePlay', ['class' => 'btn btn-main-red']);
                }
                ?>
            </p>
        </div>
    </div>
</div>

<div class="site-index-banners">
    <div style="color: #ebebeb; text-align: center">
        <p>We donate from our profit to UN Refugee Agency, you can do the same - just click the banner!</p>
    </div>

    <div class="row">
        <div align=left class="col-xs-12 col-md-4" style="class: img-responsive">
            <p>
                <a href="http://donate.unhcr.org/international/general" title="UN Refugee Agency" target='_blank'>
                    <img src="/img/banners/UNHCR/WRD2015-300x250-V2-Yadira.jpg" alt="UN Refugee Agency" >
                </a>
            </p>
        </div>
        <div align=center class="hidden-xs col-md-4" style="class: img-responsive">
            <p>
                <a href="http://www.unhcr.org" title="UN Refugee Agency" target='_blank'>
                    <img src="/img/banners/UNHCR/WRD2015-300x250-V2-Ibrahim.jpg" alt="UN Refugee Agency" >
                </a>
            </p>
        </div>
        <div align=right class="hidden-xs col-md-4" style="class: img-responsive">
            <p>
                <a href="http://www.unhcr.org/refugeeday" title="UN Refugee Agency" target='_blank'>
                    <img src="/img/banners/UNHCR/WRD2015-300x250-V2-Hany.jpg" alt="UN Refugee Agency" >
                </a>
            </p>
        </div>
    </div>

</div>


