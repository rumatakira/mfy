<?php
$headers = Yii::$app->response->headers;
$headers->add('Cache-Control', 'private, max-age=0, no-cache, no-store, must-revalidate, proxy-revalidate, post-check=0, pre-check=0');
$headers->add('Pragma', 'no-cache');
$headers->add('Expires', 0);

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\UpdatePhotoForm */

use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;

require_once __DIR__ . '/../../data/country_list.php';

$this->title = 'Setup photo';
$this->params['breadcrumbs'][] = ['label' => 'Profile', 'url' => ['profile']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-updatePhoto">
    <ul class="nav nav-tabs" role="tablist">
        <li><?= Html::a('Edit profile', ['edit-profile']) ?></li>
        <li class="active"><a role="tab" data-toggle="tab">Setup photo</a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content" align=justify>
        <div class="row">
            <div class="col-xs-12 col-md-8">
                <div align=center>
                    </br><?= Html::img("data:image/png;base64, $userPhoto", ['class' => 'img-responsive']) ?>
                    </br><?= Html::a('Delete current photo', ['delete-photo'], ['class' => 'btn btn-danger']) ?>
                </div>

                <div>
                    <p><?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
                        <?=
                        $form->field($model, 'photoFile')->widget(FileInput::classname(), [
                            'options' => ['accept' => 'image/*'],
                            'pluginOptions' => [
                                'allowedFileExtensions' => [
                                    'jpg', 'jpeg', 'png'
                                ],
                            ],
                        ])->label('Or choose the photo file on your computer for upload and replacement of your current photo :')->hint('Note! Only jpg, jpeg, png file types are allowed. The width cannot be smaller than 550 pixels. Maximum size - unlimited. You photo automatically will be resized to fit our format.')
                        ?>
                        <?= Html::a('Cancel', ['edit-profile'], ['class' => 'btn btn-main-white']) ?>
                        <?php ActiveForm::end(); ?></p>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                </br>Please use your real photo, because people want to play with people, not with abstract pictures. ;)) Also we will moderate your photo in short time. About photo rules <?= Html::a(' read here.', '@mafiaSiteRules', ['target' => '_blank']) ?>&nbsp;Briefly - no internet pics, without face, violence, harassment, humiliation, minors, porn, total nudity, copyright infringement and so on. After the third warning about your photo - we will delete your account and block the new registration. This is acceptable examples of user photos:
                <p><?php
                    for ($i = 0; $i <= 11; ++$i) {
                        $items[$i] = [
                            'href' => Yii::getAlias('@web/img/') . 'photo_example' . "$i" . '.' . 'jpg',
                            'type' => 'image/jpg',
                        ];
                    }
                    ?>
                    <?= dosamigos\gallery\Carousel::widget(['items' => $items, 'json' => true, 'clientEvents' => ['onslide' => 'function(index, slide) { console.log(slide);}', 'slideshowInterval' => 2000]]); ?></p>
                <p>We are using different photo border colors to identify users sex: <font color='#FF99CC'> woman,</font><font color='#0066FF'> man,</font></font><font color='#CC00FF'> people of other genders.</font> You can't change this setting.</p>
            </div>
        </div>
    </div>
</div>
