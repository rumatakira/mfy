<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <div class="row">
        <div class="col-xs-12 col-md-8">
            <p>
                If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.
            </p>
            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

            <?= $form->field($model, 'name') ?>
            <?= $form->field($model, 'email') ?>
            <?= $form->field($model, 'subject') ?>
            <?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>
            <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), ['template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>'])->label('Please enter verification code:')->hint('Click on image to refresh it if necessary and your answer is not cAsE sEnSiTiVe. :-)') ?>

            <div class="form-group">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-main-red', 'name' => 'contact-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div align=right class="col-xs-12 col-md-4" style="class: img-responsive">
            <p>
                <a href="http://www.unhcr.org/refugeeday" title="UN Refugee Agency" target='_blank'>
                    <img src="/img/banners/UNHCR/WRD2015-300x600-V2-Yadira.jpg" alt="UN Refugee Agency" >
                </a>
            </p>
        </div>
    </div>

</div>
