<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <div class="row">
        <div class="col-xs-12 col-md-8">
            <p>Please fill out the following fields to login:</p>
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
            <?= $form->field($model, 'username')->hint('You can use your email instead.') ?>
            <?= $form->field($model, 'password')->passwordInput() ?>

            <div style="color:#999;margin:1em 0">
                If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.
            </div>

            <?php if ($model->isVerifyRobotRequired) : ?>
                <?= $form->field($model, 'verificationCodeCaptcha')->widget(Captcha::classname())->label('Please enter verification code:')->hint('Click on image to refresh it if necessary and your answer is not cAsE sEnSiTiVe. :-)') ?>
            <?php endif; ?>

            <?= $form->field($model, 'rememberMe')->checkbox() ?>

            <div class="form-group">
                <?= Html::submitButton('Login', ['class' => 'btn btn-main-red', 'name' => 'login-button']) ?>
                <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-main-white']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div align=right class="col-xs-12 col-md-4" style="class: img-responsive">
            <p>
                <a href="http://donate.unhcr.org/international/general" title="UN Refugee Agency" target='_blank'>
                    <img src="/img/banners/UNHCR/WRD2015-300x600-V2-Yadira.jpg" alt="UN Refugee Agency" >
                </a>
            </p>
        </div>
    </div>
</div>
