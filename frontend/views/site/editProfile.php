<?php
$headers = Yii::$app->response->headers;
$headers->add('Cache-Control', 'private, max-age=0, no-cache, no-store, must-revalidate, proxy-revalidate, post-check=0, pre-check=0');
$headers->add('Pragma', 'no-cache');
$headers->add('Expires', 0);

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\EditProfileForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;

require_once __DIR__ . '/../../data/country_list.php';

$this->title = 'Edit Profile';
$this->params['breadcrumbs'][] = ['label' => 'Profile', 'url' => ['profile']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-editProfile">

    <!-- Tab panes -->
    <div class="tab-content" align=justify>
        <div class="row">
            <div class="col-xs-12 col-md-8">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a role="tab" data-toggle="tab">Edit profile</a></li>
                    <li><?= Html::a('Setup photo', ['update-photo']) ?></li>
                </ul>
                <p></br>Fill out the following fields with real information, because it inevitably will be checked through plastic cards and various confirmations, otherwise, partly functionality of the site will not be available to you.</p>

                <p><?php $form = ActiveForm::begin(['id' => 'form-editProfile']); ?>
                    <?= $form->field($model, 'username')->label('Your username:')->hint('You can use letters, numbers and underscores only, max 32 characters.') ?>
                    <?= $form->field($model, 'email')->label('Your valid email:')->hint('Note! If this email will be changed - activation link will be send to the new email and you will be logged out until the new email would be confirmed.') ?>
                    <?= $form->field($model, 'firstName')->label('Your first name:') ?>
                    <?= $form->field($model, 'lastName')->label('Your last name:') ?>
                    <?= $form->field($model, 'sex')->radioList(['Female' => 'Female', 'Man' => 'Man', 'Other gender' => 'Other gender'])->label('You are:')->hint('Note! If you\'ll change your gender - your profile photo will be deleted and your should to setup it again.') ?>
                    <?= $form->field($model, 'birthDate')->label('Your birthday in format YYYY-MM-DD, you can type or choose from widget:')->hint('Note! You must be 12+ age to play the game.')->widget(DatePicker::classname(), ['dateFormat' => 'yyyy-MM-dd', 'clientOptions' => ['yearRange' => '-100:-12', 'minDate' => '-100y', 'maxDate' => '-12y', 'changeMonth' => 'true', 'changeYear' => 'true', 'autoSize' => 'true']])->textInput() ?>
                    <?= $form->field($model, 'country')->dropDownList($countryListArray)->label('Your country:') ?>
                    <?= $form->field($model, 'area')->label('Your state, region or area:') ?>
                    <?= $form->field($model, 'city')->label('The name of your city / town / village:') ?>
                <p><?= $form->field($model, 'password')->passwordInput()->label('Please enter your current password to save changes:') ?></p>

                <div class="form-group">
                    <?= Html::submitButton('Update', ['class' => 'btn btn-main-red', 'name' => 'editProfile-button']) ?>
                    <?= Html::a('Cancel', ['profile'], ['class' => 'btn btn-main-white']) ?>
                </div>
                <?php ActiveForm::end(); ?></p>
            </div>
            <div align=right class="col-xs-12 col-md-4" style="class: img-responsive">
                <p>
                    <a href="http://donate.unhcr.org/international/general" title="UN Refugee Agency" target='_blank'>
                        <img src="/img/banners/UNHCR/WRD2015-300x600-V2-Yadira.jpg" alt="UN Refugee Agency" >
                    </a>
                </p>
            </div>
            <div align=right class="hidden-x col-md-4" style="class: img-responsive">
                <p>
                    <a href="http://www.unhcr.org" title="UN Refugee Agency" target='_blank'>
                        <img src="/img/banners/UNHCR/WRD2015-300x600-V2-Katia.jpg" alt="UN Refugee Agency" >
                    </a>
                </p>
            </div>
        </div>
    </div>
</div>
