<?php
$headers = Yii::$app->response->headers;
$headers->add('Cache-Control', 'private, max-age=0, no-cache, no-store, must-revalidate, proxy-revalidate, post-check=0, pre-check=0');
$headers->add('Pragma', 'no-cache');
$headers->add('Expires', 0);

/* @var $this yii\web\View */

use yii\bootstrap\Html;
use yii\widgets\DetailView;

$this->title = "$model->username profile:";
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-view">
    <div class="row">
        <div class="col-xs-12 col-md-8">
            <div align=center>
                <?= Html::img("data:image/png;base64, $model->photo", ['alt' => 'User photo', 'class' => 'img-responsive']) ?>
            </div>
            <div>
                </br>
                <?=
                DetailView::widget([
                    'options' => [
                        'class' => 'table table-bordered'
                    ],
                    'model' => $model,
                    'attributes' => [
                        'username',
                        'email:email',
                        [
                            'attribute' => 'status',
                            'value' => Html::tag('font', Html::encode($GLOBALS['userStatus']), ['color' => $GLOBALS['statusColor']]),
                            'format' => 'raw',
                        ],
                        'created_at:datetime',
                        'updated_at:datetime',
                        'first_name',
                        'last_name',
                        'sex',
                        'birth_date:date',
                        'country',
                        'area',
                        'city',
                    ],
                ])
                ?>
            </div>
            <div class="form-group">
                <p><?=
                    Html::a('Edit profile', ['edit-profile'], [
                        'class' => 'btn btn-main-red',])
                    ?>
                    <?= Html::a('Home', ['index'], ['class' => 'btn btn-main-white']) ?>
                    <?=
                    Html::a('Delete account', ['delete-account'], [
                        'class' => 'btn btn-danger',
                    /*            'data' => [
                      'confirm' => 'Are you sure that you want to delete your account?',
                      'method' => 'post',
                      ],
                     */                    ])
                    ?></p>
            </div>
        </div>
        <div align=right class="col-xs-12 col-md-4" style="class: img-responsive">
            <p>
                <a href="http://donate.unhcr.org/international/general" title="UN Refugee Agency" target='_blank'>
                    <img src="/img/banners/UNHCR/WRD2015-300x600-V2-Nader.jpg" alt="UN Refugee Agency" >
                </a>
            </p>
        </div>

        <div align=right class="hidden-xs col-md-4" style="class: img-responsive">
            <p>
                <a href="http://www.unhcr.org" title="UN Refugee Agency" target='_blank'>
                    <img src="/img/banners/UNHCR/WRD2015-300x600-V2-Katia.jpg" alt="UN Refugee Agency" >
                </a>
            </p>
        </div>
    </div>
</div>