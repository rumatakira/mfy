<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\Settings */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Notifications and messages settings';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-settings">
    <!-- Tab panes -->
    <div class="tab-content" align=justify>
        <div class="row">
            <div class="col-xs-12 col-md-8">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a role="tab" data-toggle="tab">Your notifications and messages setting</a></li>
                    <li><?= Html::a('Change password', ['change-password']) ?></li>
                </ul>
                </br><?php $form = ActiveForm::begin(['id' => 'form-settings']); ?>

                <?= $form->field($model, 'notifications')->label('Please choose how often you want to receive notifications from us to your email:')->radioList(['Always' => 'Always', 'Daily' => 'Daily', 'Weekly' => 'Weekly', 'Never' => 'Never']) ?>

                <?= $form->field($model, 'notificationsFrom')->label('Please choose from who you want to receive messages:')->radioList(['All users' => 'All users', 'Friends' => 'Participants']) ?>

                <?= $form->field($model, 'showEmail')->label('Please choose will your email be shown and searchable:')->radioList([0 => 'No', 1 => 'Yes']) ?>

                <div class="form-group">
                    <?= Html::submitButton('Update', ['class' => 'btn btn-main-red', 'name' => 'settings-button']) ?>
                    <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-main-white']) ?>
                </div>
                </p>

                <?php ActiveForm::end(); ?>

            </div>
            <div align=right class="col-xs-12 col-md-4" style="class: img-responsive">
                <p>
                    <a href="http://donate.unhcr.org/international/general" title="UN Refugee Agency" target='_blank'>
                        <img src="/img/banners/UNHCR/WRD2015-300x600-V2-Yaquob.jpg" alt="UN Refugee Agency" >
                    </a>
                </p>
            </div>
        </div>
    </div>
</div>
