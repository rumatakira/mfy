<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
$generatedPassword = \common\traits\CommonTrait::generatePassword();
?>
<div class="site-reset-password" align=justify>
    <div class="row">
        <div class="col-xs-12 col-md-8">
            <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

            <?= $form->field($model, 'password')->passwordInput()->label('Please choose new password:')->hint("At least 8 characters, At least 8 characters, must include at least one upper case letter, one lower case letter, and one numeric digit. We have generated a new unique strong password for you [inside brackets]: [$generatedPassword] you can copy-paste it, without brackets of course ;). If you'll use it - please save it somewhere in your secure notes.") ?>
            <?= $form->field($model, 'passwordCheck')->passwordInput()->label('Please re-enter new password:') ?>
            <?= $form->field($model, 'verificationCodeCaptcha')->widget(Captcha::classname())->label('Please enter verification code:')->hint('Click on image to refresh it if necessary and your answer is not cAsE sEnSiTiVe. :-)') ?>


            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-main-red']) ?>
                <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-main-white']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div align=right class="col-xs-12 col-md-4" style="class: img-responsive">
            <p>
                <a href="http://www.unhcr.org" title="UN Refugee Agency" target='_blank'>
                    <img src="/img/banners/UNHCR/WRD2015-300x600-V2-Katia.jpg" alt="UN Refugee Agency" >
                </a>
            </p>
        </div>
    </div>
</div>
