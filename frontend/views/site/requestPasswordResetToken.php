<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-request-password-reset" align=justify>
    <div class="row">
        <div class="col-xs-12 col-md-8">
            <p>
                Please fill out your email. A link to reset password will be sent there.
            </p>
            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

            <?= $form->field($model, 'email') ?>
            <div class="form-group">
                <?= Html::submitButton('Send', ['class' => 'btn btn-main-red']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div align=right class="col-xs-12 col-md-4" style="class: img-responsive">
            <p>
                <a href="http://donate.unhcr.org/international/general" title="UN Refugee Agency" target='_blank'>
                    <img src="/img/banners/UNHCR/WRD2015-300x250-V2-Nader.jpg" alt="UN Refugee Agency" >
                </a>
            </p>
        </div>
    </div>
</div>
