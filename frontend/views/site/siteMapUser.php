<?php

use yii\helpers\Html;

$this->title = 'Site map';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-siteMap" style="color: #FFFFFF">
    <div class="body-content">
        <div class="row">
            <div class="col-xs-12 col-md-12">

                <p><?=
                    Html::a('Play the game &raquo', '@mafiaGamePlay', [
                        'class' => 'btn btn-main-red',])
                    ?> enter to the gameplay</p>

                <p><?=
                    Html::a('Home &raquo', ['index'], [
                        'class' => 'btn btn-main-red',])
                    ?> home page</p>

                <p><?= Html::button('About', ['class' => 'btn btn-main-red']) ?></p>
                <p><li>
                    <?= Html::a('Our mission &raquo', '@mafiaSiteMission', ['class' => 'btn btn-main-red', 'target' => '_blank'])
                    ?> describes our mission
                </li></p>
                <p><li>
                    <?= Html::a('About us &raquo', '@mafiaSiteAbout', ['class' => 'btn btn-main-red', 'target' => '_blank'])
                    ?> informations about us
                </li></p>
                <p><li>
                    <?= Html::button('Site map', ['class' => 'btn btn-main-red']) ?> this page
                </li></p>

                <p><?= Html::button('Rules', ['class' => 'btn btn-main-red']) ?></p>
                <p><li>
                    <?= Html::a('Site rules &raquo', '@mafiaSiteRules', ['class' => 'btn btn-main-red', 'target' => '_blank'])
                    ?> our site and gameplay rules
                </li></p>
                <p><li>
                    <?= Html::a('Game rules &raquo', '@mafiaGameRules', ['class' => 'btn btn-main-red', 'target' => '_blank'])
                    ?> common "Mafia" role game rules
                </li></p>

                <p><?= Html::button('Contact us', ['class' => 'btn btn-main-red']) ?></p>
                <p><li>
                    <?= Html::a('Contact us &raquo', '@mafiaSiteForum', ['class' => 'btn btn-main-red', 'target' => '_blank'])
                    ?> you will be redirected to the game forum, were you can contact us
                </li></p>
                <p><li>
                    <?= Html::a('Report a bug &raquo', '@mafiaSiteBugs', ['class' => 'btn btn-main-red', 'target' => '_blank'])
                    ?> you will be redirected to the our bug tracking system, were you can create an issue and report a bug
                </li></p>

                <p><?= Html::button('Account', ['class' => 'btn btn-main-red']) ?></p>
                <p><li>
                    <?= Html::a('Profile &raquo', ['profile'], ['class' => 'btn btn-main-red'])
                    ?> here you can view and edit your profile, upload and change your photo, also you can delete your account
                </li></p>
                <p><li>
                    <?= Html::a('Settings &raquo', ['settings'], ['class' => 'btn btn-main-red'])
                    ?> here you can manage your account settings like email notifications and incoming messaging auditory
                </li></p>
                <p><li>
                    <?= Html::a('Participants &raquo', ['participants/index'], ['class' => 'btn btn-main-red'])
                    ?> here you can manage list of your participants, also you can search for and block/unblock users
                </li></p>
                <p><li>
                    <?= Html::a('Messages &raquo', ['messages'], ['class' => 'btn btn-main-red'])
                    ?> communicate with other users from here
                </li></p>
                <p><li>
                    <?= Html::a('Forum &raquo', '@mafiaSiteForum', ['class' => 'btn btn-main-red', 'target' => '_blank'])
                    ?> you will be redirected to the game forum, were are all rules, news, discussions, also only here you can contact us
                </li></p>
                <p><li>
                    <?= Html::a('Logout &raquo', ['/site/logout'], ['class' => 'btn btn-main-red', 'data' => ['method' => 'post']])
                    ?> you will be logged out the site
                </li></p>
            </div>
        </div>
    </div>
</div>
