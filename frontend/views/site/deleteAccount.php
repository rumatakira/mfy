<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\DeleteAccountForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Delete account';
$this->params['breadcrumbs'][] = ['label' => 'Profile', 'url' => ['profile']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-deleteAccount">
    <div class="row">
        <div class="col-xs-12 col-md-8">
            <p>
                Fill out the following fields (all are required) to delete your account.
            </p>
            <?php $form = ActiveForm::begin(['id' => 'form-deleteAccount']); ?>
            <?= $form->field($model, 'password')->passwordInput()->label('Please enter your current password:') ?>
            <b><?php echo $model->secretQuestion; ?></b>
            <?= $form->field($model, 'secretQuestionAnswer')->label('Please answer the above question:') ?>
            <?= $form->field($model, 'verificationCodeCaptcha')->widget(Captcha::classname())->label('Please enter verification code:')->hint('Click on image to refresh it if necessary and your answer is not cAsE sEnSiTiVe. :-)') ?>

            <div class="form-group">
                <?= Html::submitButton('Delete account', ['class' => 'btn btn-main-red', 'name' => 'delete-account-button']) ?>
                <?= Html::a('Cancel', ['profile'], ['class' => 'btn btn-main-white']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div align=right class="col-xs-12 col-md-4" style="class: img-responsive">
            <p>
                <a href="http://www.unhcr.org/refugeeday" title="UN Refugee Agency" target='_blank'>
                    <img src="/img/banners/UNHCR/WRD2015-300x600-V2-Ibrahim.jpg" alt="UN Refugee Agency" >
                </a>
            </p>
        </div>
    </div>
</div>
