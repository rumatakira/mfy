<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\jui\DatePicker;

require_once __DIR__ . '/../../data/country_list.php';

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
$generatedPassword = \common\traits\CommonTrait::generatePassword();
?>
<div class="site-signup" align=justify>
    <div class="row">
        <div class="col-xs-12 col-md-8">
            <p>
                Fill out the following fields (all are required) with real information, because it inevitably will be checked through plastic cards and various confirmations, otherwise, partly functionality of the site will not be available to you.
            </p>
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
            <?= $form->field($model, 'username')->label('Please choose username:')->hint('You can use letters, numbers, dots, dashes and underscores only, max 32 characters.') ?>
            <?= $form->field($model, 'email')->label('Please enter your valid email:')->hint('We will send activation link to this email.') ?>
            <?= $form->field($model, 'password')->passwordInput()->label('Please choose password:')->hint("At least 8 characters, must include at least one upper case letter, one lower case letter, and one numeric digit. We have generated a new unique strong password for you [inside brackets]: [$generatedPassword] you can copy-paste it, without brackets of course ;). If you'll use it - please save it somewhere in your secure notes.") ?>
            <?= $form->field($model, 'passwordCheck')->passwordInput()->label('Please re-enter password:') ?>
            <?= $form->field($model, 'firstName')->label('Your first name?') ?>
            <?= $form->field($model, 'lastName')->label('Your last name?') ?>
            <?= $form->field($model, 'sex')->radioList(['Female' => 'Female', 'Man' => 'Man', 'Other gender' => 'Other gender'])->label('You are:') ?>
            <?= $form->field($model, 'birthDate')->label('Your birthday in format YYYY-MM-DD, you can type or choose from widget:')->hint('Note! You must be 16+ age to play the game.')->
            widget(DatePicker::classname(), [
                'dateFormat' => 'yyyy-MM-dd', 
                'clientOptions' => [
                    'yearRange' => '-100:-16', 
                    'minDate' => '-100y', 
                    'maxDate' => '-16y', 
                    'changeMonth' => 'true', 
                    'changeYear' => 'true', 
                    'autoSize' => 'true'
                ],
            ])->textInput() ?>
            <?= $form->field($model, 'country')->dropDownList($countryListArray)->label('In what country do you live?') ?>
            <?= $form->field($model, 'area')->label('Enter your state, region or area:') ?>
            <?= $form->field($model, 'city')->label('Please enter the name of your city / town / village:') ?>
            <?= $form->field($model, 'secretQuestion')->radioList(['Your mother maiden name?' => 'Your mother maiden name?', 'Your favorite meal?' => 'Your favorite meal?'])->label('Please choose secret question:') ?>
            <?= $form->field($model, 'secretQuestionAnswer')->label('Please answer the secret question:')->hint('Note! Remember the answer exactly (case sensitive) as you wrote it - it is important! 255 characters max.') ?>
            <?= $form->field($model, 'verificationCodeCaptcha')->widget(Captcha::classname())->label('Please enter verification code:')->hint('Click on image to refresh it if necessary and your answer is not cAsE sEnSiTiVe. :-)') ?>

            <div class="form-group">
                <?= Html::submitButton('Signup', ['class' => 'btn btn-main-red', 'name' => 'signup-button']) ?>
                <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-main-white']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
        <div align=right class="col-xs-12 col-md-4" style="class: img-responsive">
            <p>
                <a href="http://www.unhcr.org" title="UN Refugee Agency" target='_blank'>
                    <img src="/img/banners/UNHCR/WRD2015-300x600-V2-Ibrahim.jpg" alt="UN Refugee Agency" >
                </a>
            </p>
        </div>
        <div align=right class="hidden-xs col-md-4" style="class: img-responsive">
            <p>
                <a href="http://www.unhcr.org" title="UN Refugee Agency" target='_blank'>
                    <img src="/img/banners/UNHCR/WRD2015-300x600-V2-Katia.jpg" alt="UN Refugee Agency" >
                </a>
            </p>
        </div>
        <div align=right class="hidden-xs col-md-4" title="UN Refugee Agency" style="class: img-responsive">
            <p>
                <a href="http://www.unhcr.org/refugeeday" target='_blank'>
                    <img src="/img/banners/UNHCR/WRD2015-300x600-V2-Yaquob.jpg" alt="UN Refugee Agency" >
                </a>
            </p>
        </div>
    </div>
</div>
