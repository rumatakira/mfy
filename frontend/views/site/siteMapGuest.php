<?php

use yii\helpers\Html;

$this->title = 'Site map';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-siteMap" style="color: #FFFFFF">
    <div class="body-content">
        <div class="row">
            <div class="col-xs-12 col-md-12">

                <p><?=
                    Html::a('Play the game &raquo', ['signup'], [
                        'class' => 'btn btn-main-red',])
                    ?> enter to the gameplay - you will be redirected to the signup page</p>

                <p><?=
                    Html::a('Home &raquo', ['index'], [
                        'class' => 'btn btn-main-red',])
                    ?> home page</p>

                <p><?= Html::button('About', ['class' => 'btn btn-main-red']) ?></p>
                <p><li>
                    <?= Html::a('Our mission &raquo', '@mafiaSiteMission', ['class' => 'btn btn-main-red', 'target' => '_blank'])
                    ?> describes our mission
                </li></p>
                <p><li>
                    <?= Html::a('About us &raquo', '@mafiaSiteAbout', ['class' => 'btn btn-main-red', 'target' => '_blank'])
                    ?> informations about us
                </li></p>
                <p><li>
                    <?= Html::button('Site map', ['class' => 'btn btn-main-red']) ?> this page
                </li></p>

                <p><?= Html::button('Rules', ['class' => 'btn btn-main-red']) ?></p>
                <p><li>
                    <?= Html::a('Site rules &raquo', '@mafiaSiteRules', ['class' => 'btn btn-main-red', 'target' => '_blank'])
                    ?> our site and gameplay rules
                </li></p>
                <p><li>
                    <?= Html::a('Game rules &raquo', '@mafiaGameRules', ['class' => 'btn btn-main-red', 'target' => '_blank'])
                    ?> common "Mafia" role game rules
                </li></p>

                <p><?= Html::button('Contact us', ['class' => 'btn btn-main-red']) ?></p>
                <p><li>
                    <?= Html::a('Guest contact form &raquo', ['contact'], ['class' => 'btn btn-main-red'])
                    ?> If you have business inquiries or other questions, please contact us through this form
                </li></p>

                <p><?=
                    Html::a('Signup &raquo', ['signup'], [
                        'class' => 'btn btn-main-red',])
                    ?> signup form for new users</p>

                <p><?=
                    Html::a('Login &raquo', ['login'], [
                        'class' => 'btn btn-main-red',])
                    ?> login form for existing users</p>
            </div>
        </div>
    </div>
</div>
