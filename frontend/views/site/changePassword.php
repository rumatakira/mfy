<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ChangePasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Change password';
$this->params['breadcrumbs'][] = $this->title;
$generatedPassword = \common\traits\CommonTrait::generatePassword();
?>
<div class="site-changet-password" align=justify>
    <div class="row">
        <div class="col-xs-12 col-md-8">
            <ul class="nav nav-tabs" role="tablist">
                <li><?= Html::a('Notifications and messages settings', ['settings']) ?></li>
                <li class="active"><a role="tab" data-toggle="tab">Change password</a></li>
            </ul>


            <?php $form = ActiveForm::begin(['id' => 'change-password-form']); ?>

            <?= $form->field($model, 'newPassword')->passwordInput()->label('Please choose new password:')->hint("At least 8 characters, must include at least one upper case letter, one lower case letter, and one numeric digit. We have generated a new unique strong password for you [inside brackets]: [$generatedPassword] you can copy-paste it, without brackets of course ;). If you'll use it - please save it somewhere in your secure notes.") ?>
            <?= $form->field($model, 'passwordCheck')->passwordInput()->label('Please re-enter new password:') ?>
            <?= $form->field($model, 'password')->passwordInput()->label('Please enter the current password:') ?>
            <?= $form->field($model, 'verificationCodeCaptcha')->widget(Captcha::classname())->label('Please enter verification code:')->hint('Click on image to refresh it if necessary and your answer is not cAsE sEnSiTiVe. :-)') ?>


            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-main-red']) ?>
                <?= Html::a('Cancel', ['settings'], ['class' => 'btn btn-main-white']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div align=right class="col-xs-12 col-md-4" style="class: img-responsive">
            <p>
                <a href="http://donate.unhcr.org/international/general" title="UN Refugee Agency" target='_blank'>
                    <img src="/img/banners/UNHCR/WRD2015-300x600-V2-Yadira.jpg" alt="UN Refugee Agency" >
                </a>
            </p>
        </div>
    </div>
</div>
