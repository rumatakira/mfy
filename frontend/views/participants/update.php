<?php
/* @var $this yii\web\View */
/* @var $model app\models\ParticipantsForm */

$this->params['breadcrumbs'][] = ['label' => 'Your participants', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update participant:';
?>

<div class="participants-form-update">
    <div class="row">
        <div class="col-xs-12 col-md-8">

            <?=
            $this->render('_form', [
                'model' => $model,
                'redirectFromUsersSearch' => $redirectFromUsersSearch,
            ])
            ?>

        </div>
        <div align=right class="col-xs-12 col-md-4" style="class: img-responsive">
            <p>
                <a href="http://donate.unhcr.org/international/general" title="UN Refugee Agency" target='_blank'>
                    <img src="/img/banners/UNHCR/WRD2015-300x600-V2-Ibrahim.jpg" alt="UN Refugee Agency" >
                </a>
            </p>
        </div>
    </div>
</div>
