<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ParticipantsForm */

$this->params['breadcrumbs'][] = ['label' => 'Your participants', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'View participant:';
?>

<div class="participants-form-view">
    <div class="row">
        <div class="col-xs-12 col-md-8">
            <div align=center>
                <?= Html::img("data:image/png;base64, $userPhoto", ['alt' => 'User photo', 'class' => 'img-responsive']) ?>
            </div>
            <div>
                </br>

                <?php if ($model->user->show_email == 0) : ?>

                    <?=
                    DetailView::widget([
                        'options' => [
                            'class' => 'table table-bordered'
                        ],
                        'model' => $model->user,
                        'attributes' => [
                            'username',
                            [
                                'attribute' => 'status',
                                'value' => Html::tag('font', Html::encode($GLOBALS['userStatus']), ['color' => $GLOBALS['statusColor']]),
                                'format' => 'raw',
                            ],
                            'created_at:datetime',
                            'updated_at:datetime',
                            'first_name',
                            'last_name',
                            'sex',
                            'birth_date:date',
                            'country',
                            'area',
                            'city',
                        ],
                    ])
                    ?>

                <?php else : ?>

                    <?=
                    DetailView::widget([
                        'options' => [
                            'class' => 'table table-bordered'
                        ],
                        'model' => $model->user,
                        'attributes' => [
                            'username',
                            'email:email',
                            [
                                'attribute' => 'status',
                                'value' => Html::tag('font', Html::encode($GLOBALS['userStatus']), ['color' => $GLOBALS['statusColor']]),
                                'format' => 'raw',
                            ],
                            'created_at:datetime',
                            'updated_at:datetime',
                            'first_name',
                            'last_name',
                            'sex',
                            'birth_date:date',
                            'country',
                            'area',
                            'city',
                        ],
                    ])
                    ?>

                <?php endif; ?>
            </div>
            <div>
                <?=
                DetailView::widget([
                    'options' => [
                        'class' => 'table table-bordered'
                    ],
                    'model' => $model,
                    'attributes' => [
                        'participant',
                        'category',
                    ],
                ])
                ?>
            </div>

            <div class="form-group">
                <p>
                    <?= Html::a('Update', ['update', 'id' => $model->id, 'redirectFromUsersSearch' => $redirectFromUsersSearch], ['class' => 'btn btn-main-red']) ?>
                    <?=
                    Html::a('Delete', ['delete', 'id' => $model->id, 'redirectFromUsersSearch' => $redirectFromUsersSearch], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ])
                    ?>
                    
                    <?php if (!$redirectFromUsersSearch): ?>
                        <?= Html::a('Cancel', ['/participants/index'], ['class' => 'btn btn-main-white']) ?>
                    <?php else: ?>
                        <?= Html::a('Cancel', ['/search-users/index'], ['class' => 'btn btn-main-white']) ?>
                    <?php endif; ?>
                    
                </p>
            </div>
        </div>
        <div align=right class="col-xs-12 col-md-4" style="class: img-responsive">
            <p>
                <a href="http://donate.unhcr.org/international/general" title="UN Refugee Agency" target='_blank'>
                    <img src="/img/banners/UNHCR/WRD2015-300x600-V2-Nader.jpg" alt="UN Refugee Agency" >
                </a>
            </p>
        </div>
        <div align=right class="hidden-xs col-md-4" style="class: img-responsive">
            <p>
                <a href="http://www.unhcr.org" title="UN Refugee Agency" target='_blank'>
                    <img src="/img/banners/UNHCR/WRD2015-300x600-V2-Katia.jpg" alt="UN Refugee Agency" >
                </a>
            </p>
        </div>
    </div>
</div>
