<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\SearchParticipants */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Your participants:';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="participants-form-index" align=justify style="width:100%">
    <p>All users in the game divided in to two groups - your participants (blocked users also calculates as participants with category 'Blocked'), and all other users. User can be only in one of this groups at the same time.</p>
    <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a role="tab" data-toggle="tab">Your participants</a></li>
        <li><?= Html::a('Search users', ['search-users/index']) ?></li>
    </ul>
    </br>
    <p>You can order results ascending or descending by pressing to the title links. To find participant(s) - type (can be grouped and not ended) in the blank fields, after each field press Enter. To reset form - press Reset button. To view user profile - press 'eye' tool, to block user - change his or her category to blocked or Blocked by pressing 'pen' tool. In same manner to unblock user - just change or delete category. Note! If you'll delete blocked user from participants he/she will be unblocked.</p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-bordered'
        ],
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
            ],
            [
                'attribute' => 'user.photo',
                'format' => 'raw',
                'value' => function($data) {
                    $tmp = $data->user->photo;
                    $id = $data->id;
                    return Html::a(Html::img("data:image/png;base64, $tmp", ['alt' => 'User photo', 'class' => 'img-responsive']), ['/participants/view', 'id' => $id]);
                },
            ],
            'participant',
            'category',
            'user.first_name',
            'user.last_name',
            'user.country:ntext',
            'user.area:ntext',
            'user.city',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Tools',
                'template' => '{view}&nbsp{update}&nbsp{message}&nbsp{delete}',
                'buttons' => [
                    'message' => function ($url, $model) {
                        return Html::a(
                        '<span class="glyphicon glyphicon-comment" title="Message"></span>', $url);
                    },
                ],
            ],
        ],
    ]);
    ?>

    <p>
        <?= Html::a('Create new participant', ['create'], ['class' => 'btn btn-main-red']) ?>
        <?= Html::a('Reset', ['index'], ['class' => 'btn btn-main-white']) ?>
        <?= Html::a('Cancel', ['/site/index'], ['class' => 'btn btn-main-white']) ?>
    </p>
</div>
