<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ParticipantsForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="participants-form-form">
    <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a role="tab" data-toggle="tab">You participants</a></li>
        <li><?= Html::a('Find participants', ['search-users/index']) ?></li>
    </ul></br>
    <?php $form = ActiveForm::begin(); ?>

    <?php if ($model->isNewRecord): ?>
        <?= $form->field($model, 'participant')->label('Participant username:')->textInput(['maxlength' => true]) ?>
    <?php endif; ?>

    <?= $form->field($model, 'category')->textInput(['maxlength' => true])->label('Please enter category for the participant ' . $model->participant . ':')->hint('If the field will remain blank the default category "Uncategorized" will be assigned.') ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-main-red']) ?>
        
        <?php if (!$redirectFromUsersSearch): ?>
            <?= Html::a('Cancel', ['/participants/index'], ['class' => 'btn btn-main-white']) ?>
        <?php else: ?>
            <?= Html::a('Cancel', ['/search-users/index'], ['class' => 'btn btn-main-white']) ?>
        <?php endif; ?>
    
    </div>

    <?php ActiveForm::end(); ?>

</div>
