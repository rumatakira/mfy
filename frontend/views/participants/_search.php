<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\SearchParticipants */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="participants-form-search">

    <?php
    $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]);
    ?>

    <?= $form->field($model, 'participant') ?>

    <?= $form->field($model, 'category') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-main-red']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-main-white']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
