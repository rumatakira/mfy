<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SearchUsers */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Search users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="search-users-index" align=justify style="width:100%">
    <p>All users in the game divided in to two groups - your participants (blocked users also calculates as participants with category 'Blocked'), and all other users. User can be only in one of this groups at the same time.</p>
    <ul class="nav nav-tabs" role="tablist">
        <li><?= Html::a('Your participants', ['participants/index']) ?></li>
        <li class="active"><a role="tab" data-toggle="tab">Search users</a></li>
    </ul>

    </br>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    </br>
    <p>You can order results ascending or descending by pressing to the title links. To find users - type (can be grouped and not ended) in the blank fields, after each field press Enter. To reset form - press Reset button. To view user profile - press 'eye' tool, to add user to participants - press [+] ('add') tool. To block user - press [Ø] ('block') tool. To unblock user - just change or delete his/her category in your <?= Html::a('participants', ['participants/index']) ?>, also deletion from participants unblock user.</p>

    <h1><?= Html::encode('Search users') ?></h1>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-bordered'
        ],
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
            ],
            [
                'attribute' => 'photo',
                'format' => 'raw',
                'value' => function($data) {
                    return Html::a(Html::img("data:image/png;base64, $data->photo", ['alt' => 'User photo', 'class' => 'img-responsive']), ['/search-users/view', 'id' => $data->id]);
                },
            ],
            'username',
            //'email:email',
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:Y-m-d'],
            ],
            'first_name',
            'last_name',
            'sex',
            'birth_date',
            'country:ntext',
            'area:ntext',
            'city',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Tools',
                'template' => '{view}&nbsp{add}&nbsp{block}&nbsp{message}',
                'buttons' => [
                    'add' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-plus" title="Add"></span>', $url);
                    },
                    'block' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-ban-circle" title="Block"></span>', $url);
                    },
                    'message' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-comment" title="Message"></span>', $url);
                    },
                ],
            ],
        ],
    ]);
    ?>
    <p>
        <?= Html::a('Reset', ['index'], ['class' => 'btn btn-main-white']) ?>
        <?= Html::a('Cancel', ['/site/index'], ['class' => 'btn btn-main-white']) ?>
        <?= Html::a('View only premium users', ['index', 'viewOnlyPremium' => 'true'], ['class' => 'btn btn-main-white']) ?>
        <?= Html::a('View only users with approved photo', ['index', 'viewOnlyApprovedPhoto' => 'true'], ['class' => 'btn btn-main-white']) ?>
        <?= Html::a('View only users with photo', ['index', 'viewOnlyWithPhoto' => 'true'], ['class' => 'btn btn-main-white']) ?>
    </p>
</div>