<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SearchUserForm */

$this->params['breadcrumbs'][] = ['label' => 'Users list', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'View user:';
?>

<div class="search-users-view">
    <div class="row">
        <div class="col-xs-12 col-md-8">
            <div align=center>
                <?= Html::img("data:image/png;base64, $model->photo", ['alt' => 'User photo', 'class' => 'img-responsive']) ?>
            </div>
            <div>
                </br>

                <?php if ($model->show_email == 0) : ?>

                    <?=
                    DetailView::widget([
                        'options' => [
                            'class' => 'table table-bordered'
                        ],
                        'model' => $model,
                        'attributes' => [
                            'username',
                            [
                                'attribute' => 'status',
                                'value' => Html::tag('font', Html::encode($GLOBALS['userStatus']), ['color' => $GLOBALS['statusColor']]),
                                'format' => 'raw',
                            ],
                            'created_at:datetime',
                            'updated_at:datetime',
                            'first_name',
                            'last_name',
                            'sex',
                            'birth_date:date',
                            'country',
                            'area',
                            'city',
                        ],
                    ])
                    ?>

                <?php else : ?>

                    <?=
                    DetailView::widget([
                        'options' => [
                            'class' => 'table table-bordered'
                        ],
                        'model' => $model,
                        'attributes' => [
                            'username',
                            'email:email',
                            [
                                'attribute' => 'status',
                                'value' => Html::tag('font', Html::encode($GLOBALS['userStatus']), ['color' => $GLOBALS['statusColor']]),
                                'format' => 'raw',
                            ],
                            'created_at:datetime',
                            'updated_at:datetime',
                            'first_name',
                            'last_name',
                            'sex',
                            'birth_date:date',
                            'country',
                            'area',
                            'city',
                        ],
                    ])
                    ?>

                <?php endif; ?>
            </div>
            <div class="form-group">
                <p>
                    <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-main-white']) ?>
                    <?= Html::a('Add to participants', ['add', 'id' => $model->id], ['class' => 'btn btn-main-white']) ?>
                </p>
            </div>
        </div>
        <div align=right class="col-xs-12 col-md-4" style="class: img-responsive">
            <p>
                <a href="http://donate.unhcr.org/international/general" title="UN Refugee Agency" target='_blank'>
                    <img src="/img/banners/UNHCR/WRD2015-300x600-V2-Nader.jpg" alt="UN Refugee Agency" >
                </a>
            </p>
        </div>
        <div align=right class="hidden-xs col-md-4" style="class: img-responsive">
            <p>
                <a href="http://www.unhcr.org" title="UN Refugee Agency" target='_blank'>
                    <img src="/img/banners/UNHCR/WRD2015-300x600-V2-Katia.jpg" alt="UN Refugee Agency" >
                </a>
            </p>
        </div>
    </div>
</div>
