<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SearchUsersForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="search-users-by-email">

    <?php
    $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]);
    ?>

    <?= $form->field($model, 'email')->label('There are a lot of ways to find participants. First of all - you can search user by email:')->hint('Note! Not all users made their email viewable and searchable in the ' . Html::a('settings', ['site/settings']) . '. You can use more complex form below to find users.') ?>

    <div class="form-group">
        <?= Html::submitButton('Search user by email', ['class' => 'btn btn-main-red']) ?>
        <?= Html::a('Reset', ['index'], ['class' => 'btn btn-main-white']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
